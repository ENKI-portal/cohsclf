/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: HO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * HO2Gas implementation
 * $Id: HO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "HO2Gas.h"

static ShomateData HO2lt = {"HO2 gas","HO2",33.00674,
			    26.00960,34.85810,-16.30060,3.110441,-0.018611,
			    -7.140991,250.7660,2.092001};
static ShomateData HO2ht = {"HO2 gas","HO2",33.00674,
			    45.87510,8.814350,-1.636031,0.098053,-10.17380,
			    -26.90210,266.5260,2.092001};

HO2Gas::HO2Gas() {
  setTk(298.15);
}
HO2Gas::HO2Gas(double ltk) {
  setTk(ltk);
}
void HO2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<2000.0) {
    setData(&HO2lt);
  }
  else {
    setData(&HO2ht);
  }
  return;
}
    
