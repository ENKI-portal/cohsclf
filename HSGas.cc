/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: HSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * HSGas implementation
 * $Id: HSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "HSGas.h"

static ShomateData HSlt = {"HS gas","HS",33.07394,
			   38.04306,-27.46792,34.06462,-11.79875,-0.009743,
			   128.8961,248.3945,139.3272};
static ShomateData HSht = {"HS gas","HS",33.07394,
			   32.99507,2.841514,-0.507766,0.038247,-2.909667,
			   124.4870,230.0066,139.3272};

HSGas::HSGas() {
  setTk(298.15);
}
HSGas::HSGas(double ltk) {
  setTk(ltk);
}
void HSGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&HSlt);
  }
  else {
    setData(&HSht);
  }
  return;
}
    
