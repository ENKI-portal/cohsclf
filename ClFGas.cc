/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClFGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClFGas implementation
 * $Id: ClFGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClFGas.h"

static ShomateData ClFlt = {"ClF gas","ClF",54.4510,
			    33.00963,7.780901,-4.389142,0.894945,-0.258478,
			    -61.31694,254.2747,-50.29210};
static ShomateData ClFht = {"ClF gas","ClF",54.4510,
			    400.6418,-226.2720,49.98708,-3.436085,-341.4424,
			    -626.3866,327.8775,-50.29210};
// note: static in this case makes it local to this file

ClFGas::ClFGas() {
  setTk(298.15);
}

ClFGas::ClFGas(double ltk) {
  setTk(ltk);
}

void ClFGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<2300.0) {
    setData(&ClFlt);
  }
  else {
    setData(&ClFht);
  }
  return;
}




