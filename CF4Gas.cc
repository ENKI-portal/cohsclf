/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CF4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CF4Gas implementation
 * $Id: CF4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CF4Gas.h"

static ShomateData CF4lt = {"CF4 gas","CF4",88.0030,
			    15.96778,210.3318,-189.4657,62.20227,-0.217317,
			    -946.4877,224.6766,-933.1994};
static ShomateData CF4ht = {"CF4 gas","CF4",88.0030,
			    106.2221,1.076122,-0.223192,0.015753,-8.340679,
			    -987.7755,355.9764,-933.1994};
// note: static in this case makes it local to this file

CF4Gas::CF4Gas() {
  setTk(298.15);
}

CF4Gas::CF4Gas(double ltk) {
  setTk(ltk);
}

void CF4Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&CF4lt);
  }
  else {
    setData(&CF4ht);
  }
  return;
}



