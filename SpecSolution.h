/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: SpecSolution.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#ifndef SPECSOLUTION_H
#define SPECSOLUTION_H

#include <float.h>
#include <stdio.h>
#include "Solution.h"

/**
 Generic class to deal with associated solutions.  
 In generic case ideal mixing assumed.
 First ncomp species MUST correspond to components. nmoles is total
 of component moles.  This will NOT correspond to sum of species moles.
 version 1.1 added RAND algorithm.  Moved some stuff from SpecSolNID.
 @author Victor Kress
 @version $Revision: 1.1.1.1 $
 */
class SpecSolution: public Solution {
 public:
  /// generic constructor.
  SpecSolution();
  /// generic destructor.
  virtual ~SpecSolution();

  // persistent phase properties ///////////
  /// returns number of species
  int getNspec(void);    
  /// returns pointer to species name
  virtual char *getSpecName(int i);
  /// returns pointer to species formula
  virtual char *getSpecFormula(int i);

  // setting methods //////////////////////
  /// set phase temperature in Kelvins
  virtual void setTk(double ltk);
  /** set total moles. This is sum of component moles. Sum of species
      moles will be different, and dependent on speciation.
  */
  virtual void setMoles(double m);
  /** Set component vector in moles. 
      Species filled but not equilibrated. */ 
  virtual void setComps(double *lcomps);
  /** Set species vector in moles.
      Components set automatically. Not automatically equilibrated.
      Only non-zero species will be included in calculations. */
  virtual void setSpecs(double *lspecs);
  /// like setSpecs except all potential species are activated
  virtual void setSpecsFull(double *lspecs);

  // getting methods ///////////////////////
  /// Get phase species abundances in moles
  virtual void getSpecs(double *lspecs);

  // Gibbs and derivatives
  /// standard-state chemical potential at T and P. 
  virtual double getMu0(int ispec);
  /// chemical potential of component in J/mole. Calls getCompMu() 
  virtual double getSpecMu(int i);

  // utility functions ////////////////////////
  /// Calculates homogenious equilibrium.  Must be invoked manually.
  /// @return number of iterations to convergence
  virtual int speciate(double tol);
  /// Calculates homogenious equilibrium. Invoked manually. Depreciated.
  /// @return number of iterations to convergence
  virtual int speciate();

 protected:
  /// indexing array used to eliminate unused reactions. r is recalculated
  /// from lcomp and lspec as needed.
  int *rdx;
  /// species chemical potentials
  double *mu;
  /// generic working space
  double **m1,**m2,**m3,*v1;
  /**
     Guts of speciation routine. Ideal solution assumed.
     Implements RAND speciation algorithm as presented in:
     Smith and Missen (1982) Chemical Reaction Equilibrium Analysis:
     Theory and Algorithms. Krieger.
     Gf is vector of standard state chemical potentials at T
  */
  virtual int speciate_function(double tol);
  /// Vector of species moles.
  double *specs;
  /// sum of species moles
  double specSum;
  /// Species stoichiometries [nspec][ncomp].First ncomp rows MUST be identity.
  double **spstoich;    
  /// reaction stoich. matrix. Indexed on non-zero species and reactions
  double **nu;
  /// space allocation and variable initialization
  virtual int init();
  /// transfers comps to specs
  virtual int fillSpecs();
  /// transfers specs to comps
  virtual void fillComps();
  /// index non-zero species
  virtual void index();

 private:
  /// tolerance for speciation
  double spectol;
  /// speciation variables and arrays
  double *rhs,*psi,*b,*bm,*delta;
};

#endif






