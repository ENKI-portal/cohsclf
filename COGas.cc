/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * COGas implementation
 */

#include "COGas.h"

static ShomateData COlt = {"CO gas","CO",28.0104,
			   25.56759,6.096130,4.054656,-2.671301,0.131021,
			   -118.0089,227.3665,-110.5271};
static ShomateData COht = {"CO gas","CO",28.0104,
			   35.15070,1.300095,-0.205921,0.013550,-3.282780,
			   -127.8375,231.7120,-110.5271};

COGas::COGas() {
  setTk(298.15);
}
COGas::COGas(double ltk) {
  setTk(ltk);
}
void COGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1300.0) {
    setData(&COlt);
  }
  else {
    setData(&COht);
  }
  return;
}
    
