/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CO2Gas implementation
 */

#include "CO2Gas.h"

static ShomateData CO2lt = {"CO2 gas","CO2",44.0098,
			    24.99735,55.18696,-33.69137,7.948387,-0.136638,
			    -403.6075,228.2431,-393.5224};
static ShomateData CO2ht = {"CO2 gas","CO2",44.0098,
			    58.16639,2.720074,-0.492289,0.038844,-6.447293,
			    -425.9186,263.6125,-393.5224};

CO2Gas::CO2Gas() {
  setTk(298.15);
}
CO2Gas::CO2Gas(double ltk) {
  setTk(ltk);
}
void CO2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&CO2lt);
  }
  else {
    setData(&CO2ht);
  }
  return;
}
    
