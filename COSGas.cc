/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * COSGas implementation
 * $Id: COSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "COSGas.h"

static ShomateData COSlt = {"COS gas","COS",60.0764,
			    34.53892,43.05378,-26.61773,6.338844,-0.327515,
			    -151.5001,259.8118,-138.4071};
static ShomateData COSht = {"COS gas","COS",60.0764,
			    60.32240,1.738332,-0.209982,0.014110,-5.128873,
			    -168.6307,287.6454,-138.4071};

COSGas::COSGas() {
  setTk(298.15);
}
COSGas::COSGas(double ltk) {
  setTk(ltk);
}
void COSGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&COSlt);
  }
  else {
    setData(&COSht);
  }
  return;
}
    
