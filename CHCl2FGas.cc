/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CHCl2FGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CHCl2FGas implementation
 * $Id: CHCl2FGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CHCl2FGas.h"

static ShomateData CHCl2Flt = {"CHCl2F gas","CHCl2F",102.9229,
			       29.01600,149.8437,-121.8335,37.34889,-0.255977,
			       -298.4267,287.3366,-283.2572};
static ShomateData CHCl2Fht = {"CHCl2F gas","CHCl2F",102.9229,
			       100.2976,4.162565,-0.816980,0.055361,-9.890600,
			       -337.5028,380.1373,-283.2572};
// note: static in this case makes it local to this file

CHCl2FGas::CHCl2FGas() {
  setTk(298.15);
}

CHCl2FGas::CHCl2FGas(double ltk) {
  setTk(ltk);
}

void CHCl2FGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1100.0) {
    setData(&CHCl2Flt);
  }
  else {
    setData(&CHCl2Fht);
  }
  return;
}




