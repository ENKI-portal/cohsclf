/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2OH4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2OH4Gas implementation
 */

#include "C2OH4Gas.h"

static ShomateData C2OH4lt = {"ethylene oxide gas","C2OH4",44.0526,
			      -23.25802,275.6997,-188.9729,51.03350,0.386930,
			      -55.09156,142.7777,-52.63514};
static ShomateData C2OH4ht = {"C2OH4 gas","C2OH4",44.0526,
			      131.3483,13.80594,-2.645062,0.175820,-30.03639,
			      -158.3795,313.4276,-52.63514};

C2OH4Gas::C2OH4Gas() {
  setTk(298.15);
}
C2OH4Gas::C2OH4Gas(double ltk) {
  setTk(ltk);
}
void C2OH4Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&C2OH4lt);
  }
  else {
    setData(&C2OH4ht);
  }
  return;
}
    
