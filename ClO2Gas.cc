/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClO2Gas implementation
 * $Id: ClO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClO2Gas.h"

static ShomateData ClO2lt = {"ClO2 gas","ClO2",67.4510,
			     24.12473,81.01898,-73.80702,24.54401,-0.033548,
			     94.29481,265.1367,104.6000};
static ShomateData ClO2ht = {"ClO2 gas","ClO2",67.4510,
			     57.43293,1.212997,-0.106451,0.007852,-2.773703,
			     79.71022,314.8933,104.6000};
// note: static in this case makes it local to this file

ClO2Gas::ClO2Gas() {
  setTk(298.15);
}

ClO2Gas::ClO2Gas(double ltk) {
  setTk(ltk);
}

void ClO2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&ClO2lt);
  }
  else {
    setData(&ClO2ht);
  }
  return;
}



