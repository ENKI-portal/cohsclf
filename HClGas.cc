/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: HClGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * HClGas implementation
 * $Id: HClGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "HClGas.h"

static ShomateData HCllt = {"HCl gas","HCl",36.4609,
			    32.12392,-13.45805,19.86852,-6.853936,-0.049672,
			    -101.6206,228.6866,-92.31201};
static ShomateData HClht = {"HCl gas","HCl",36.4609,
			    31.91923,3.203184,-0.541539,0.035925,-3.438525,
			    -108.0150,218.2768,-92.31201};

HClGas::HClGas() {
  setTk(298.15);
}
HClGas::HClGas(double ltk) {
  setTk(ltk);
}
void HClGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&HCllt);
  }
  else {
    setData(&HClht);
  }
  return;
}
    
