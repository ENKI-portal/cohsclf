/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Solution.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/* Solution class header file
 * Generic class to deal with phases which are solutions
 * Victor Kress
 */

#ifndef SOLUTION_H
#define SOLUTION_H

#include "Phase.h"

/**
   solution class.
   Generic class for Phases which are simple solutions.
   In generic implementation, ideal mixing is assumed.
   Converted to extensive properties 4/14/03
   $Id: Solution.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
   @author Victor Kress
   @version $Revision: 1.1.1.1 $
 */
class Solution: public Phase {
 public:
  /// Generic constructor. Assumes STP.
  Solution();
  /// destructor
  virtual ~Solution();

  // persistent phase properties //////
  /// Returns number of components in system
  int getNcomp(void);
  /// Returns molecular weight of component i
  virtual double getMW(int i);
  /// Returns gram formula weight of solution
  virtual double getMW();   // overrides Phase
  /// gets component number for input string.  Match must be exact.
  /// @param lab char* to name of phase
  /// @return integer index or -1 if not found.
  virtual int getCompNo(char *lab);
  /// Returns pointer to component name. 
  /// Caution: string destroyed with Solution object.
  virtual char *getCompName(int i);
  /// Returns pointer to component formula. 
  /// Caution: string destroyed with Solution object.
  virtual char *getCompFormula(int i);

  // setting methods ///////////////////
  /// set temperature in Kelvins
  virtual void setTk(double ltk); //overrides Phase
  /// set moles. In Solution and subclasses, 
  /// nmoles is total of component moles.
  virtual void setMoles(double m);
  /// Set component moles.  Coppies values to internal array.
  virtual void setComps(double *rawcomps);

  // getting methods //////////////////
  /// returns sum of moles of components
  double getMoles();
  /// Returns component moles in caller-allocated vector.
  virtual void getComps(double *rawcomps);

  /// returns single standard state chemical potential in J/mole
  virtual double getMu0(int icomp);
  /// standard state chemical potential in Joules.
  /// @return potential of all components in joules.
  virtual void getMu0(double *m0);
  /// Returns chemical potential in J/mole.
  virtual double getMu(int i);
  /// chemical potential in Joules.
  /// @return potential of all components in joules.
  virtual void getMu(double *m);

 protected:
  /// number of species. In Solutions set equal to ncomp in init().
  int nspec;
  /// number of non-zero components. Set in index().
  int lcomp;
  /// number of non-zero species. In Solutions ALWAYS equal to lcomp.
  int lspec;
  /// indexing array used to eliminate zero species
  int *indx;
  /// vector of components
  double *comps; 
  /// vector of standard state properties
  Phase **ss;
  /// working space for SVD calculations
  double **A,**v,*w;
  /// index non-zero species
  virtual void index();
  /// space allocation and variable initialization
  virtual int init();
};

#endif





