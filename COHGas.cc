/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * COHGas implementation
 * $Id: COHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "COHGas.h"

static ShomateData COHlt = {"COH gas","COH",29.01834,
			    21.13803,40.43610,-14.71337,0.969010,0.239639,
			    36.34712,240.1695,43.51402};
static ShomateData COHht = {"COH gas","COH",29.01834,
			    52.79371,2.666155,-0.392339,0.023808,-7.457018,
			    11.37797,267.2798,43.51402};

COHGas::COHGas() {
  setTk(298.15);
}
COHGas::COHGas(double ltk) {
  setTk(ltk);
}
void COHGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&COHlt);
  }
  else {
    setData(&COHht);
  }
  return;
}
    
