/*
 *   This file is part of a software package for calculating homogeneous equilibrium in 
 *   C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COHSClFGasSol.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include <stdlib.h>
#include <string.h>
#include "COHSClFGasSol.h"
#include "CGas.h"
#include "O2Gas.h"
#include "H2Gas.h"
#include "S2Gas.h"
#include "SGas.h"
#include "S3Gas.h"
#include "COGas.h"
#include "CO2Gas.h"
#include "SO2Gas.h"
#include "SOGas.h"
#include "S2OGas.h"
#include "COSGas.h"
#include "CSGas.h"
#include "CS2Gas.h"
#include "HGas.h"
#include "OHGas.h"
#include "HO2Gas.h"
#include "H2OGas.h"
#include "H2O2Gas.h"
#include "H2SO4Gas.h"
#include "H2SGas.h"
#include "CHGas.h"
#include "COHGas.h"
#include "CH2Gas.h"
#include "COH2Gas.h"
#include "CH3Gas.h"
#include "CH4Gas.h"
#include "C2HGas.h"
#include "C2H2Gas.h"
#include "C2H4Gas.h"
#include "C2OH4Gas.h"
#include "C2OGas.h"
#include "C3O2Gas.h"
#include "HSGas.h"
#include "SO3Gas.h"
#include "Cl2Gas.h"
#include "F2Gas.h"
#include "HClGas.h"
#include "Cl2OGas.h"
#include "ClOGas.h"
#include "ClO2Gas.h"
#include "Cl2SGas.h"
#include "Cl2S2Gas.h"
#include "ClSGas.h"
#include "ClS2Gas.h"
#include "C2Cl2Gas.h"
#include "C2Cl4Gas.h"
#include "CCl4Gas.h"
#include "H2F2Gas.h"
#include "HFGas.h"
#include "F2OGas.h"
#include "FOGas.h"
#include "F2SGas.h"
#include "F2S2Gas.h"
#include "S2F2Gas.h"
#include "F3SGas.h"
#include "F4SGas.h"
#include "C2F2Gas.h"
#include "C2F4Gas.h"
#include "CF4Gas.h"
#include "CCl2F2Gas.h"
#include "CCl3FGas.h"
#include "CClF3Gas.h"
#include "CClFOGas.h"
#include "CHCl2FGas.h"
#include "CHClF2Gas.h"
#include "ClFGas.h"
#include "ClFO2SGas.h"

COHSClFGasSol::COHSClFGasSol():NCOMP(6),NSPEC(68),CDEX(0),O2DEX(1),H2DEX(2),
			       S2DEX(3),CL2DEX(4),F2DEX(5),SDEX(6),S3DEX(7), 
			       CODEX(8),CO2DEX(9),SO2DEX(10),SODEX(11),
			       S2ODEX(12),COSDEX(13),CSDEX(14),CS2DEX(15),
			       HDEX(16),OHDEX(17),HO2DEX(18),H2ODEX(19),
			       H2O2DEX(20),H2SO4DEX(21),H2SDEX(22),CHDEX(23),
			       COHDEX(24),CH2DEX(25),COH2DEX(26),CH3DEX(27),
			       CH4DEX(28),C2HDEX(29),C2H2DEX(30),C2H4DEX(31),
			       C2OH4DEX(32),C2ODEX(33),C3O2DEX(34),HSDEX(35),
			       SO3DEX(36),HCLDEX(37),CL2ODEX(38),CLODEX(39),
			       CLO2DEX(40),CL2SDEX(41),CL2S2DEX(42),CLSDEX(43),
			       CLS2DEX(44),C2CL2DEX(45),C2CL4DEX(46),CCL4DEX(47
			       ),H2F2DEX(48),HFDEX(49),F2ODEX(50),FODEX(51),
			       F2SDEX(52),F2S2DEX(53),S2F2DEX(54),F3SDEX(55),
			       F4SDEX(56),C2F2DEX(57),C2F4DEX(58),CF4DEX(59),
			       CCL2F2DEX(60),CCL3FDEX(61),CCLF3DEX(62),
			       CCLFODEX(63),CHCL2FDEX(64),CHCLF2DEX(65),
			       CLFDEX(66),CLFO2SDEX(67) {
  init();
  return;
};

COHSClFGasSol::~COHSClFGasSol() {
  int i;
  for (i=0;i<NSPEC;i++) {
    delete ss[i];
  }
  delete []name;
  delete []formula;
  return;
};

int COHSClFGasSol::init() {
  int err,i;
  name = new char[19];
  strcpy(name,"COHSClF Gas Solution");
  formula = new char[19];
  strcpy(formula,"CwOxHySzClqFr");

  ncomp = NCOMP;
  nspec = NSPEC;

  err=SpecSolution::init();  // starts cascade of space allocation in parents

  if (err) {
    return err;
  }
  else {
    ss[CDEX] = new CGas();
    spstoich[CDEX][CDEX] = 1.;
    spstoich[CDEX][O2DEX] = 0.;
    spstoich[CDEX][H2DEX] = 0.;
    spstoich[CDEX][S2DEX] = 0.;
    spstoich[CDEX][CL2DEX] = 0.;
    spstoich[CDEX][F2DEX] = 0.;

    ss[O2DEX] = new O2Gas();
    spstoich[O2DEX][CDEX] = 0.;
    spstoich[O2DEX][O2DEX] = 1.;
    spstoich[O2DEX][H2DEX] = 0.;
    spstoich[O2DEX][S2DEX] = 0.;
    spstoich[O2DEX][CL2DEX] = 0.;
    spstoich[O2DEX][F2DEX] = 0.;
 
    ss[H2DEX] = new H2Gas();
    spstoich[H2DEX][CDEX] = 0.;
    spstoich[H2DEX][O2DEX] = 0.;
    spstoich[H2DEX][H2DEX] = 1.;
    spstoich[H2DEX][S2DEX] = 0.;
    spstoich[H2DEX][CL2DEX] = 0.;
    spstoich[H2DEX][F2DEX] = 0.;

    ss[S2DEX] = new S2Gas();
    spstoich[S2DEX][CDEX] = 0.;
    spstoich[S2DEX][O2DEX] = 0.;
    spstoich[S2DEX][H2DEX] = 0.;
    spstoich[S2DEX][S2DEX] = 1.;
    spstoich[S2DEX][CL2DEX] = 0.;
    spstoich[S2DEX][F2DEX] = 0.;

    ss[CL2DEX] = new Cl2Gas();
    spstoich[CL2DEX][CDEX] = 0.;
    spstoich[CL2DEX][O2DEX] = 0.;
    spstoich[CL2DEX][H2DEX] = 0.;
    spstoich[CL2DEX][S2DEX] = 0.;
    spstoich[CL2DEX][CL2DEX] = 1.;
    spstoich[CL2DEX][F2DEX] = 0.;

    ss[F2DEX] = new F2Gas();
    spstoich[F2DEX][CDEX] = 0.;
    spstoich[F2DEX][O2DEX] = 0.;
    spstoich[F2DEX][H2DEX] = 0.;
    spstoich[F2DEX][S2DEX] = 0.;
    spstoich[F2DEX][CL2DEX] = 0.;
    spstoich[F2DEX][F2DEX] = 1.;

    ss[SDEX] = new SGas();
    spstoich[SDEX][CDEX] = 0.;
    spstoich[SDEX][O2DEX] = 0.;
    spstoich[SDEX][H2DEX] = 0.;
    spstoich[SDEX][S2DEX] = 0.5;
    spstoich[SDEX][CL2DEX] = 0.;
    spstoich[SDEX][F2DEX] = 0.;

    ss[S3DEX] = new S3Gas();
    spstoich[S3DEX][CDEX] = 0.;
    spstoich[S3DEX][O2DEX] = 0.;
    spstoich[S3DEX][H2DEX] = 0.;
    spstoich[S3DEX][S2DEX] = 1.5;
    spstoich[S3DEX][CL2DEX] = 0.;
    spstoich[S3DEX][F2DEX] = 0.;

    ss[CODEX] = new COGas();
    spstoich[CODEX][CDEX] = 1.;
    spstoich[CODEX][O2DEX] = .5;
    spstoich[CODEX][H2DEX] = 0.;
    spstoich[CODEX][S2DEX] = 0.;
    spstoich[CODEX][CL2DEX] = 0.;
    spstoich[CODEX][F2DEX] = 0.;

    ss[CO2DEX] = new CO2Gas();
    spstoich[CO2DEX][CDEX] = 1.;
    spstoich[CO2DEX][O2DEX] = 1.;
    spstoich[CO2DEX][H2DEX] = 0.;
    spstoich[CO2DEX][S2DEX] = 0.;
    spstoich[CO2DEX][CL2DEX] = 0.;
    spstoich[CO2DEX][F2DEX] = 0.;
 
    ss[SO2DEX] = new SO2Gas();
    spstoich[SO2DEX][CDEX] = 0.;
    spstoich[SO2DEX][O2DEX] = 1.;
    spstoich[SO2DEX][H2DEX] = 0.;
    spstoich[SO2DEX][S2DEX] = .5;
    spstoich[SO2DEX][CL2DEX] = 0.;
    spstoich[SO2DEX][F2DEX] = 0.;

    ss[SODEX] = new SOGas();
    spstoich[SODEX][CDEX] = 0.;
    spstoich[SODEX][O2DEX] = .5;
    spstoich[SODEX][H2DEX] = 0.;
    spstoich[SODEX][S2DEX] = .5;
    spstoich[SODEX][CL2DEX] = 0.;
    spstoich[SODEX][F2DEX] = 0.;
 
    ss[S2ODEX] = new S2OGas();
    spstoich[S2ODEX][CDEX] = 0.;
    spstoich[S2ODEX][O2DEX] = .5;
    spstoich[S2ODEX][H2DEX] = 0.;
    spstoich[S2ODEX][S2DEX] = 1.;
    spstoich[S2ODEX][CL2DEX] = 0.;
    spstoich[S2ODEX][F2DEX] = 0.;

    ss[COSDEX] = new COSGas();
    spstoich[COSDEX][CDEX] = 1.;
    spstoich[COSDEX][O2DEX] = .5;
    spstoich[COSDEX][H2DEX] = 0.;
    spstoich[COSDEX][S2DEX] = .5;
    spstoich[COSDEX][CL2DEX] = 0.;
    spstoich[COSDEX][F2DEX] = 0.;

    ss[CSDEX] = new CSGas();
    spstoich[CSDEX][CDEX] = 1.;
    spstoich[CSDEX][O2DEX] = 0.;
    spstoich[CSDEX][H2DEX] = 0.;
    spstoich[CSDEX][S2DEX] = .5;
    spstoich[CSDEX][CL2DEX] = 0.;
    spstoich[CSDEX][F2DEX] = 0.;

    ss[CS2DEX] = new CS2Gas();
    spstoich[CS2DEX][CDEX] = 1.; 
    spstoich[CS2DEX][O2DEX] = 0.; 
    spstoich[CS2DEX][H2DEX] = 0.; 
    spstoich[CS2DEX][S2DEX] = 1.; 
    spstoich[CS2DEX][CL2DEX] = 0.; 
    spstoich[CS2DEX][F2DEX] = 0.; 

    ss[HDEX] = new HGas();
    spstoich[HDEX][CDEX] = 0.;
    spstoich[HDEX][O2DEX] = 0.;
    spstoich[HDEX][H2DEX] = .5;
    spstoich[HDEX][S2DEX] = 0.;
    spstoich[HDEX][CL2DEX] = 0.;
    spstoich[HDEX][F2DEX] = 0.;

    ss[OHDEX] = new OHGas();
    spstoich[OHDEX][CDEX] = 0.;
    spstoich[OHDEX][O2DEX] = .5;
    spstoich[OHDEX][H2DEX] = .5;
    spstoich[OHDEX][S2DEX] = 0.;
    spstoich[OHDEX][CL2DEX] = 0.;
    spstoich[OHDEX][F2DEX] = 0.;

    ss[HO2DEX] = new HO2Gas();
    spstoich[HO2DEX][CDEX] = 0.;
    spstoich[HO2DEX][O2DEX] = 1.;
    spstoich[HO2DEX][H2DEX] = .5;
    spstoich[HO2DEX][S2DEX] = 0.;
    spstoich[HO2DEX][CL2DEX] = 0.;
    spstoich[HO2DEX][F2DEX] = 0.;

    ss[H2ODEX] = new H2OGas();
    spstoich[H2ODEX][CDEX] = 0.;
    spstoich[H2ODEX][O2DEX] = .5;
    spstoich[H2ODEX][H2DEX] = 1.;
    spstoich[H2ODEX][S2DEX] = 0.;
    spstoich[H2ODEX][CL2DEX] = 0.;
    spstoich[H2ODEX][F2DEX] = 0.;

    ss[H2O2DEX] = new H2O2Gas();
    spstoich[H2O2DEX][CDEX] = 0.;
    spstoich[H2O2DEX][O2DEX] = 1.;
    spstoich[H2O2DEX][H2DEX] = 1.;
    spstoich[H2O2DEX][S2DEX] = 0.;
    spstoich[H2O2DEX][CL2DEX] = 0.;
    spstoich[H2O2DEX][F2DEX] = 0.;

    ss[H2SO4DEX] = new H2SO4Gas();
    spstoich[H2SO4DEX][CDEX] = 0.;
    spstoich[H2SO4DEX][O2DEX] = 2.;
    spstoich[H2SO4DEX][H2DEX] = 1.;
    spstoich[H2SO4DEX][S2DEX] = .5;
    spstoich[H2SO4DEX][CL2DEX] = 0.;
    spstoich[H2SO4DEX][F2DEX] = 0.;

    ss[H2SDEX] = new H2SGas();
    spstoich[H2SDEX][CDEX] = 0.;
    spstoich[H2SDEX][O2DEX] = 0.;
    spstoich[H2SDEX][H2DEX] = 1.;
    spstoich[H2SDEX][S2DEX] = .5;
    spstoich[H2SDEX][CL2DEX] = 0.;
    spstoich[H2SDEX][F2DEX] = 0.;

    ss[CHDEX] = new CHGas();
    spstoich[CHDEX][CDEX] = 1.;
    spstoich[CHDEX][O2DEX] = 0.;
    spstoich[CHDEX][H2DEX] = .5;
    spstoich[CHDEX][S2DEX] = 0.;
    spstoich[CHDEX][CL2DEX] = 0.;
    spstoich[CHDEX][F2DEX] = 0.;

    ss[COHDEX] = new COHGas();
    spstoich[COHDEX][CDEX] = 1.;
    spstoich[COHDEX][O2DEX] = .5;
    spstoich[COHDEX][H2DEX] = .5;
    spstoich[COHDEX][S2DEX] = 0.;
    spstoich[COHDEX][CL2DEX] = 0.;
    spstoich[COHDEX][F2DEX] = 0.;

    ss[CH2DEX] = new CH2Gas();
    spstoich[CH2DEX][CDEX] = 1.;
    spstoich[CH2DEX][O2DEX] = 0.;
    spstoich[CH2DEX][H2DEX] = 1.;
    spstoich[CH2DEX][S2DEX] = 0.;
    spstoich[CH2DEX][CL2DEX] = 0.;
    spstoich[CH2DEX][F2DEX] = 0.;

    ss[COH2DEX] = new COH2Gas();
    spstoich[COH2DEX][CDEX] = 1.;
    spstoich[COH2DEX][O2DEX] = .5;
    spstoich[COH2DEX][H2DEX] = 1.;
    spstoich[COH2DEX][S2DEX] = 0.;
    spstoich[COH2DEX][CL2DEX] = 0.;
    spstoich[COH2DEX][F2DEX] = 0.;

    ss[CH3DEX] = new CH3Gas();
    spstoich[CH3DEX][CDEX] = 1.;
    spstoich[CH3DEX][O2DEX] = 0.;
    spstoich[CH3DEX][H2DEX] = 1.5;
    spstoich[CH3DEX][S2DEX] = 0.;
    spstoich[CH3DEX][CL2DEX] = 0.;
    spstoich[CH3DEX][F2DEX] = 0.;

    ss[CH4DEX] = new CH4Gas();
    spstoich[CH4DEX][CDEX] = 1.;
    spstoich[CH4DEX][O2DEX] = 0.;
    spstoich[CH4DEX][H2DEX] = 2.;
    spstoich[CH4DEX][S2DEX] = 0.;
    spstoich[CH4DEX][CL2DEX] = 0.;
    spstoich[CH4DEX][F2DEX] = 0.;
 
    ss[C2HDEX] = new C2HGas();
    spstoich[C2HDEX][CDEX] = 2.;
    spstoich[C2HDEX][O2DEX] = 0.;
    spstoich[C2HDEX][H2DEX] = .5;
    spstoich[C2HDEX][S2DEX] = 0.;
    spstoich[C2HDEX][CL2DEX] = 0.;
    spstoich[C2HDEX][F2DEX] = 0.;

    ss[C2H2DEX] = new C2H2Gas();
    spstoich[C2H2DEX][CDEX] = 2.;
    spstoich[C2H2DEX][O2DEX] = 0.;
    spstoich[C2H2DEX][H2DEX] = 1.;
    spstoich[C2H2DEX][S2DEX] = 0.;
    spstoich[C2H2DEX][CL2DEX] = 0.;
    spstoich[C2H2DEX][F2DEX] = 0.;

    ss[C2H4DEX] = new C2H4Gas();
    spstoich[C2H4DEX][CDEX] = 2.;
    spstoich[C2H4DEX][O2DEX] = 0.;
    spstoich[C2H4DEX][H2DEX] = 2.;
    spstoich[C2H4DEX][S2DEX] = 0.;
    spstoich[C2H4DEX][CL2DEX] = 0.;
    spstoich[C2H4DEX][F2DEX] = 0.;

    ss[C2OH4DEX] = new C2OH4Gas();
    spstoich[C2OH4DEX][CDEX] = 2.;
    spstoich[C2OH4DEX][O2DEX] = .5;
    spstoich[C2OH4DEX][H2DEX] = 2.;
    spstoich[C2OH4DEX][S2DEX] = 0.;
    spstoich[C2OH4DEX][CL2DEX] = 0.;
    spstoich[C2OH4DEX][F2DEX] = 0.;

    ss[C2ODEX] = new C2OGas();
    spstoich[C2ODEX][CDEX] = 2.;
    spstoich[C2ODEX][O2DEX] = .5;
    spstoich[C2ODEX][H2DEX] = 0.;
    spstoich[C2ODEX][S2DEX] = 0.;
    spstoich[C2ODEX][CL2DEX] = 0.;
    spstoich[C2ODEX][F2DEX] = 0.;

    ss[C3O2DEX] = new C3O2Gas();
    spstoich[C3O2DEX][CDEX] = 3.;
    spstoich[C3O2DEX][O2DEX] = 1.;
    spstoich[C3O2DEX][H2DEX] = 0.;
    spstoich[C3O2DEX][S2DEX] = 0.;
    spstoich[C3O2DEX][CL2DEX] = 0.;
    spstoich[C3O2DEX][F2DEX] = 0.;

    ss[HSDEX] = new HSGas();
    spstoich[HSDEX][CDEX] = 0.;
    spstoich[HSDEX][O2DEX] = 0.;
    spstoich[HSDEX][H2DEX] = .5;
    spstoich[HSDEX][S2DEX] = .5;
    spstoich[HSDEX][CL2DEX] = 0.;
    spstoich[HSDEX][F2DEX] = 0.;

    ss[SO3DEX] = new SO3Gas();
    spstoich[SO3DEX][CDEX] = 0.;
    spstoich[SO3DEX][O2DEX] = 1.5;
    spstoich[SO3DEX][H2DEX] = 0.;
    spstoich[SO3DEX][S2DEX] = .5;
    spstoich[SO3DEX][CL2DEX] = 0.;
    spstoich[SO3DEX][F2DEX] = 0.;
 
    ss[HCLDEX] = new HClGas();
    spstoich[HCLDEX][CDEX] = 0.;
    spstoich[HCLDEX][O2DEX] = 0.;
    spstoich[HCLDEX][H2DEX] = .5;
    spstoich[HCLDEX][S2DEX] = 0.;
    spstoich[HCLDEX][CL2DEX] = .5;
    spstoich[HCLDEX][F2DEX] = 0.;

    ss[CL2ODEX] = new Cl2OGas();
    spstoich[CL2ODEX][CDEX] = 0.;
    spstoich[CL2ODEX][O2DEX] = .5;
    spstoich[CL2ODEX][H2DEX] = 0.;
    spstoich[CL2ODEX][S2DEX] = 0.;
    spstoich[CL2ODEX][CL2DEX] = 1.;
    spstoich[CL2ODEX][F2DEX] = 0.;

    ss[CLODEX] = new ClOGas();
    spstoich[CLODEX][CDEX] = 0.;
    spstoich[CLODEX][O2DEX] = .5;
    spstoich[CLODEX][H2DEX] = 0.;
    spstoich[CLODEX][S2DEX] = 0.;
    spstoich[CLODEX][CL2DEX] = .5;
    spstoich[CLODEX][F2DEX] = 0.;

    ss[CLO2DEX] = new ClO2Gas();
    spstoich[CLO2DEX][CDEX] = 0.;
    spstoich[CLO2DEX][O2DEX] = 1.;
    spstoich[CLO2DEX][H2DEX] = 0.;
    spstoich[CLO2DEX][S2DEX] = 0.;
    spstoich[CLO2DEX][CL2DEX] = .5;
    spstoich[CLO2DEX][F2DEX] = 0.;

    ss[CL2SDEX] = new Cl2SGas();
    spstoich[CL2SDEX][CDEX] = 0.;
    spstoich[CL2SDEX][O2DEX] = 0.;
    spstoich[CL2SDEX][H2DEX] = 0.;
    spstoich[CL2SDEX][S2DEX] = .5;
    spstoich[CL2SDEX][CL2DEX] = 1.;
    spstoich[CL2SDEX][F2DEX] = 0.;

    ss[CL2S2DEX] = new Cl2S2Gas();
    spstoich[CL2S2DEX][CDEX] = 0.;
    spstoich[CL2S2DEX][O2DEX] = 0.;
    spstoich[CL2S2DEX][H2DEX] = 0.;
    spstoich[CL2S2DEX][S2DEX] = 1.;
    spstoich[CL2S2DEX][CL2DEX] = 1.;
    spstoich[CL2S2DEX][F2DEX] = 0.;

    ss[CLSDEX] = new ClSGas();
    spstoich[CLSDEX][CDEX] = 0.;
    spstoich[CLSDEX][O2DEX] = 0.;
    spstoich[CLSDEX][H2DEX] = 0.;
    spstoich[CLSDEX][S2DEX] = .5;
    spstoich[CLSDEX][CL2DEX] = .5;
    spstoich[CLSDEX][F2DEX] = 0.;

    ss[CLS2DEX] = new ClS2Gas();
    spstoich[CLS2DEX][CDEX] = 0.;
    spstoich[CLS2DEX][O2DEX] = 0.;
    spstoich[CLS2DEX][H2DEX] = 0.;
    spstoich[CLS2DEX][S2DEX] = 1.;
    spstoich[CLS2DEX][CL2DEX] = .5;
    spstoich[CLS2DEX][F2DEX] = 0.;

    ss[C2CL2DEX] = new C2Cl2Gas();
    spstoich[C2CL2DEX][CDEX] = 2.;
    spstoich[C2CL2DEX][O2DEX] = 0.;
    spstoich[C2CL2DEX][H2DEX] = 0.;
    spstoich[C2CL2DEX][S2DEX] = 0.;
    spstoich[C2CL2DEX][CL2DEX] = 1.;
    spstoich[C2CL2DEX][F2DEX] = 0.;

    ss[C2CL4DEX] = new C2Cl4Gas();
    spstoich[C2CL4DEX][CDEX] = 2.;
    spstoich[C2CL4DEX][O2DEX] = 0.;
    spstoich[C2CL4DEX][H2DEX] = 0.;
    spstoich[C2CL4DEX][S2DEX] = 0.;
    spstoich[C2CL4DEX][CL2DEX] = 2.;
    spstoich[C2CL4DEX][F2DEX] = 0.;

    ss[CCL4DEX] = new CCl4Gas();
    spstoich[CCL4DEX][CDEX] = 1.;
    spstoich[CCL4DEX][O2DEX] = 0.;
    spstoich[CCL4DEX][H2DEX] = 0.;
    spstoich[CCL4DEX][S2DEX] = 0.;
    spstoich[CCL4DEX][CL2DEX] = 2.;
    spstoich[CCL4DEX][F2DEX] = 0.;

    ss[H2F2DEX] = new H2F2Gas();
    spstoich[H2F2DEX][CDEX] = 0.;
    spstoich[H2F2DEX][O2DEX] = 0.;
    spstoich[H2F2DEX][H2DEX] = 1.;
    spstoich[H2F2DEX][S2DEX] = 0.;
    spstoich[H2F2DEX][CL2DEX] = 0.;
    spstoich[H2F2DEX][F2DEX] = 1.;

    ss[HFDEX] = new HFGas();
    spstoich[HFDEX][CDEX] = 0.;
    spstoich[HFDEX][O2DEX] = 0.;
    spstoich[HFDEX][H2DEX] = .5;
    spstoich[HFDEX][S2DEX] = 0.;
    spstoich[HFDEX][CL2DEX] = 0.;
    spstoich[HFDEX][F2DEX] = .5;

    ss[F2ODEX] = new F2OGas();
    spstoich[F2ODEX][CDEX] = 0.;
    spstoich[F2ODEX][O2DEX] = .5;
    spstoich[F2ODEX][H2DEX] = 0.;
    spstoich[F2ODEX][S2DEX] = 0.;
    spstoich[F2ODEX][CL2DEX] = 0.;
    spstoich[F2ODEX][F2DEX] = 1.;

    ss[FODEX] = new FOGas();
    spstoich[FODEX][CDEX] = 0.;
    spstoich[FODEX][O2DEX] = .5;
    spstoich[FODEX][H2DEX] = 0.;
    spstoich[FODEX][S2DEX] = 0.;
    spstoich[FODEX][CL2DEX] = 0.;
    spstoich[FODEX][F2DEX] = .5;

    ss[F2SDEX] = new F2SGas();
    spstoich[F2SDEX][CDEX] = 0.;
    spstoich[F2SDEX][O2DEX] = 0.;
    spstoich[F2SDEX][H2DEX] = 0.;
    spstoich[F2SDEX][S2DEX] = .5;
    spstoich[F2SDEX][CL2DEX] = 0.;
    spstoich[F2SDEX][F2DEX] = 1.;

    ss[F2S2DEX] = new F2S2Gas();
    spstoich[F2S2DEX][CDEX] = 0.;
    spstoich[F2S2DEX][O2DEX] = 0.;
    spstoich[F2S2DEX][H2DEX] = 0.;
    spstoich[F2S2DEX][S2DEX] = 1.;
    spstoich[F2S2DEX][CL2DEX] = 0.;
    spstoich[F2S2DEX][F2DEX] = 1.;

    ss[S2F2DEX] = new S2F2Gas();
    spstoich[S2F2DEX][CDEX] = 0.;
    spstoich[S2F2DEX][O2DEX] = 0.;
    spstoich[S2F2DEX][H2DEX] = 0.;
    spstoich[S2F2DEX][S2DEX] = 1.;
    spstoich[S2F2DEX][CL2DEX] = 0.;
    spstoich[S2F2DEX][F2DEX] = 1.;

    ss[F3SDEX] = new F3SGas();
    spstoich[F3SDEX][CDEX] = 0.;
    spstoich[F3SDEX][O2DEX] = 0.;
    spstoich[F3SDEX][H2DEX] = 0.;
    spstoich[F3SDEX][S2DEX] = .5;
    spstoich[F3SDEX][CL2DEX] = 0.;
    spstoich[F3SDEX][F2DEX] = 1.5;

    ss[F4SDEX] = new F4SGas();
    spstoich[F4SDEX][CDEX] = 0.;
    spstoich[F4SDEX][O2DEX] = 0.;
    spstoich[F4SDEX][H2DEX] = 0.;
    spstoich[F4SDEX][S2DEX] = .5;
    spstoich[F4SDEX][CL2DEX] = 0.;
    spstoich[F4SDEX][F2DEX] = 2.;

    ss[C2F2DEX] = new C2F2Gas();
    spstoich[C2F2DEX][CDEX] = 2.;
    spstoich[C2F2DEX][O2DEX] = 0.;
    spstoich[C2F2DEX][H2DEX] = 0.;
    spstoich[C2F2DEX][S2DEX] = 0.;
    spstoich[C2F2DEX][CL2DEX] = 0.;
    spstoich[C2F2DEX][F2DEX] = 1.;

    ss[C2F4DEX] = new C2F4Gas();
    spstoich[C2F4DEX][CDEX] = 2.;
    spstoich[C2F4DEX][O2DEX] = 0.;
    spstoich[C2F4DEX][H2DEX] = 0.;
    spstoich[C2F4DEX][S2DEX] = 0.;
    spstoich[C2F4DEX][CL2DEX] = 0.;
    spstoich[C2F4DEX][F2DEX] = 2.;

    ss[CF4DEX] = new CF4Gas();
    spstoich[CF4DEX][CDEX] = 1.;
    spstoich[CF4DEX][O2DEX] = 0.;
    spstoich[CF4DEX][H2DEX] = 0.;
    spstoich[CF4DEX][S2DEX] = 0.;
    spstoich[CF4DEX][CL2DEX] = 0.;
    spstoich[CF4DEX][F2DEX] = 2.;

    ss[CCL2F2DEX] = new CCl2F2Gas();
    spstoich[CCL2F2DEX][CDEX] = 1.;
    spstoich[CCL2F2DEX][O2DEX] = 0.;
    spstoich[CCL2F2DEX][H2DEX] = 0.;
    spstoich[CCL2F2DEX][S2DEX] = 0.;
    spstoich[CCL2F2DEX][CL2DEX] = 1.;
    spstoich[CCL2F2DEX][F2DEX] = 1.;

    ss[CCL3FDEX] = new CCl3FGas();
    spstoich[CCL3FDEX][CDEX] = 1.;
    spstoich[CCL3FDEX][O2DEX] = 0.;
    spstoich[CCL3FDEX][H2DEX] = 0.;
    spstoich[CCL3FDEX][S2DEX] = 0.;
    spstoich[CCL3FDEX][CL2DEX] = 1.5;
    spstoich[CCL3FDEX][F2DEX] = .5;

    ss[CCLF3DEX] = new CClF3Gas();
    spstoich[CCLF3DEX][CDEX] = 1.;
    spstoich[CCLF3DEX][O2DEX] = 0.;
    spstoich[CCLF3DEX][H2DEX] = 0.;
    spstoich[CCLF3DEX][S2DEX] = 0.;
    spstoich[CCLF3DEX][CL2DEX] = .5;
    spstoich[CCLF3DEX][F2DEX] = 1.5;

    ss[CCLFODEX] = new CClFOGas();
    spstoich[CCLFODEX][CDEX] = 1.;
    spstoich[CCLFODEX][O2DEX] = .5;
    spstoich[CCLFODEX][H2DEX] = 0.;
    spstoich[CCLFODEX][S2DEX] = 0.;
    spstoich[CCLFODEX][CL2DEX] = .5;
    spstoich[CCLFODEX][F2DEX] = .5;

    ss[CHCL2FDEX] = new CHCl2FGas();
    spstoich[CHCL2FDEX][CDEX] = 1.;
    spstoich[CHCL2FDEX][O2DEX] = 0.;
    spstoich[CHCL2FDEX][H2DEX] = .5;
    spstoich[CHCL2FDEX][S2DEX] = 0.;
    spstoich[CHCL2FDEX][CL2DEX] = 1.;
    spstoich[CHCL2FDEX][F2DEX] = .5;

    ss[CHCLF2DEX] = new CHClF2Gas();
    spstoich[CHCLF2DEX][CDEX] = 1.;
    spstoich[CHCLF2DEX][O2DEX] = 0.;
    spstoich[CHCLF2DEX][H2DEX] = .5;
    spstoich[CHCLF2DEX][S2DEX] = 0.;
    spstoich[CHCLF2DEX][CL2DEX] = .5;
    spstoich[CHCLF2DEX][F2DEX] = 1.;

    ss[CLFDEX] = new ClFGas();
    spstoich[CLFDEX][CDEX] = 0.;
    spstoich[CLFDEX][O2DEX] = 0.;
    spstoich[CLFDEX][H2DEX] = 0.;
    spstoich[CLFDEX][S2DEX] = 0.;
    spstoich[CLFDEX][CL2DEX] = .5;
    spstoich[CLFDEX][F2DEX] = .5;
    
    ss[CLFO2SDEX] = new ClFO2SGas();
    spstoich[CLFO2SDEX][CDEX] = 0.;
    spstoich[CLFO2SDEX][O2DEX] = 1.;
    spstoich[CLFO2SDEX][H2DEX] = 0.;
    spstoich[CLFO2SDEX][S2DEX] = .5;
    spstoich[CLFO2SDEX][CL2DEX] = .5;
    spstoich[CLFO2SDEX][F2DEX] = .5;
    
    return 0;  // end of init
  }
}












