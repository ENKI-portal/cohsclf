/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2H4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2H4Gas implementation
 * $Id: C2H4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2H4Gas.h"

static ShomateData C2H4lt = {"Ethylene gas","C2H4",28.05376,
			     -6.387880,184.4019,-112.9718,28.49593,0.315540,
			     48.17332,163.1568,52.46694};
static ShomateData C2H4ht = {"Ethylene gas","C2H4",28.05376,
			     106.5104,13.73260,-2.628481,0.174595,-26.14469,
			     -35.36237,275.0424,52.46694};

C2H4Gas::C2H4Gas() {
  setTk(298.15);
}
C2H4Gas::C2H4Gas(double ltk) {
  setTk(ltk);
}
void C2H4Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&C2H4lt);
  }
  else {
    setData(&C2H4ht);
  }
  return;
}
    
