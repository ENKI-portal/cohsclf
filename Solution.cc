/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Solution.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * Solution class
 * $Id: Solution.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include <math.h>
#include <iostream.h>
#include <string.h>
#include <stdio.h>
#include "Solution.h"
#include "NumUtil.h"
#include "ModifiedCholesky.h"
#include "eigensystem.h"

Solution::Solution() {
  // init must be called by implementing child after ncomp set
  nspec=0;  // overridden in either subclasses or init
  return;
}
Solution::~Solution() {
  // delete constructs allocated in init()
  delete []comps;
  delete []ss;
  delete []indx;
  free_dmatrix(ncomp+1,ncomp+1,A);
  free_dmatrix(ncomp+1,ncomp+1,v);
  delete []w;
  return;
}
int Solution::init() {
  int err;
  err=Phase::init();
  if (!nspec) nspec=ncomp;
  if (err) return err;
  if (ncomp>0) {
    comps = new double[ncomp];
    ss = new (Phase*)[nspec];
    indx = new int[nspec];
    A =dmatrix(ncomp+1,ncomp+1);
    v =dmatrix(ncomp+1,ncomp+1);
    w = new double[ncomp+1];
    return 0;
  }
  else {
    return 1;
  }
}


// persistent phase properties ////////////////
int Solution::getNcomp() {
  return ncomp;
}
double Solution::getMW(int i) {
  return ss[i]->getMW();
}
double Solution::getMW() {
  int i;
  double gfw;
  gfw=0.;
  for (i=0;i<ncomp;i++) {
    gfw += comps[i]*ss[i]->getMW()/nmoles;
  }
  return gfw;
}
char *Solution::getCompName(int i) {
  if (i<0||i>ncomp) {
    return NULL;
  }
  else {
    return ss[i]->name;
  }
}
char *Solution::getCompFormula(int i) {
  if (i<0||i>ncomp) {
    return NULL;
  }
  else {
    return ss[i]->formula;
  }
}
int Solution::getCompNo(char *lab) {
  int i;
  for (i=0;i<ncomp;i++) {
    if (strcmp(getCompName(i),lab)==0) {
      return i;
    }
  }
  return -1;
}

// setting methods ///////////////////////////
void Solution::setTk(double ltk) {
  int i;
  tk=ltk;

  for (i=0;i<ncomp;i++) {
    ss[i]->setTk(tk);
  }
  return;
}
void Solution::setMoles(double m) {
  int i;
  double sum;
  nmoles = m;
  for (i=0,sum=0.0;i<ncomp;i++) {
    sum += comps[i];
  }
  for (i=0;i<ncomp;i++) {
    comps[i] *= nmoles/sum;
  }
  return;
}
void Solution::setComps(double *lcomps) {
  int i;
  for (i=0,nmoles=0.;i<ncomp;i++) {
    comps[i]=lcomps[i];
    nmoles += comps[i];
  }
  index();
  getMoles(); // to set nmoles
  return;
}

// getting methods ////////////////////////
double Solution::getMoles() {
  int i;
  double m;
  for (i=0,m=0.;i<ncomp;i++) {
    m += comps[i];
  }
  nmoles = m;
  return m;
}

void Solution::getComps(double *lcomps) {
  int i;
  for (i=0;i<ncomp;i++) {
    lcomps[i]=comps[i];
  }
  return;
}

// Gibbs and derivatives
double Solution::getMu0(int i) {
  if (i>=0&&i<ncomp) {
    return ss[i]->getGibbs();
  }
  else {
    cout << "\ncomp index out of range error in Solution::getmu0";
    return 0.;
  }
}
void Solution::getMu0(double *m0) {
  int i;
  for (i=0;i<ncomp;i++) {
    m0[i]=getMu0(i);
  }
  return;
}
double Solution::getMu(int icomp) {
  // Assuming ideal solution for now
  double mu;
  if (icomp<0||icomp>ncomp) {
    cout << "\nSolution getMu recieved out of range component number";
    return 0.;
  }
  else {
    mu = (comps[icomp]>0.)? getMu0(icomp) + R*tk*log(comps[icomp]/nmoles) : 0.;
    return mu;
  } 
}
void Solution::getMu(double *mu) {
  int i;
  for (i=0;i<ncomp;i++) {
    mu[i]=getMu(i);
  }
  return;
}

// utility functions /////////////////////
void Solution::index() {
  int i;
  for (i=lcomp=lspec=0;i<ncomp;i++) {
    if (comps[i]>0.0) {
      indx[lcomp] = i;
      lcomp++;
    }
  }
  lspec=lcomp;
  return;
}


