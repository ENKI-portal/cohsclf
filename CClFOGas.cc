/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CClFOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CClFOGas implementation
 * $Id: CClFOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CClFOGas.h"

static ShomateData CClFOlt = {"CClFO gas","CClFO",82.4610,
			      20.33554,154.9896,-168.6324,72.84093,-0.096916,
			      -438.7008,261.7176,-426.7680};
static ShomateData CClFOht = {"CClFO gas","CClFO",82.4610,
			      77.10024,4.023795,-0.928882,0.071069,-4.126374,
			      -462.0433,350.7041,-426.7680};
// note: static in this case makes it local to this file

CClFOGas::CClFOGas() {
  setTk(298.15);
}

CClFOGas::CClFOGas(double ltk) {
  setTk(ltk);
}

void CClFOGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<600.0) {
    setData(&CClFOlt);
  }
  else {
    setData(&CClFOht);
  }
  return;
}




