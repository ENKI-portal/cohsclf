/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2F2Gas implementation
 * $Id: C2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2F2Gas.h"

static ShomateData C2F2lt = {"C2F2 gas","C2F2",62.0180,
			     38.31795,89.70831,-65.98963,17.89903,-0.244157,
			     5.236527,265.0840,20.9200};
static ShomateData C2F2ht = {"C2F2 gas","C2F2",62.0180,
			     85.46531,0.982972,-0.192525,0.013000,-6.812054,
			     -21.55835,323.6060,20.92004};
// note: static in this case makes it local to this file

C2F2Gas::C2F2Gas() {
  setTk(298.15);
}

C2F2Gas::C2F2Gas(double ltk) {
  setTk(ltk);
}

void C2F2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1200.0) {
    setData(&C2F2lt);
  }
  else {
    setData(&C2F2ht);
  }
  return;
}



