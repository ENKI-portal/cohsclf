/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: H2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * H2Gas implementation
 */

#include "H2Gas.h"

static ShomateData h2lt = {"H2 gas","H2",2.01588,
			 33.10780,-11.50800,11.60930,-2.844400,-.159665,
			 -9.991971,172.7880,0.0};
static ShomateData h2ht = {"H2 gas","H2",2.01588,
			   34.1434,.503927,.372036,-.038599,-8.074761,
			   -21.2188,162.093,0.0};

H2Gas::H2Gas() {
  setTk(298.15);
}
H2Gas::H2Gas(double ltk) {
  setTk(ltk);
}
void H2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1500.0) {
    setData(&h2lt);
  }
  else {
    setData(&h2ht);
  }
  return;
}
    
