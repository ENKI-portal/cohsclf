/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Cl2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * Cl2OGas implementation
 * $Id: Cl2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "Cl2OGas.h"

static ShomateData Cl2O = {"Cl2O gas","Cl2O",86.9050,
			   56.28944,1.617418,-0.436174,0.037109,-0.819554,
			   68.26573,330.9406,87.86400};
// note: static in this case makes it local to this file

Cl2OGas::Cl2OGas() {
  setData(&Cl2O);
}

Cl2OGas::Cl2OGas(double ltk) {
  setData(&Cl2O);
  setTk(ltk);
}




