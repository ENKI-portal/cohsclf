/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: H2SO4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * H2SO4Gas implementation
 * $Id: H2SO4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "H2SO4Gas.h"

static ShomateData H2SO4lt = {"H2SO4 gas","H2SO4",98.07948,
			      47.28924,190.3314,-148.1299,43.86631,-0.740016,
			      -758.9525,301.2961,-735.1288};
static ShomateData H2SO4ht = {"H2SO4 gas","H2SO4",98.07948,
			      139.2289,9.513663,-1.795577,0.118069,-15.61486,
			      -813.0976,416.1854,-735.1288};

H2SO4Gas::H2SO4Gas() {
  setTk(298.15);
}
H2SO4Gas::H2SO4Gas(double ltk) {
  setTk(ltk);
}
void H2SO4Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&H2SO4lt);
  }
  else {
    setData(&H2SO4ht);
  }
  return;
}
    
