/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2HGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2HGas implementation
 * $Id: C2HGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2HGas.h"

static ShomateData C2Hlt = {"Ethynyl gas","C2H",25.02994,
			    33.40916,24.11331,-6.600595,0.278912,-0.259459,
			    465.1311,239.5148,476.9760};
static ShomateData C2Hht = {"Ethynyl gas","C2H",25.02994,
			    39.29220,13.32081,-1.952364,0.086290,0.978203,
			    464.3027,248.5949,476.9760};

C2HGas::C2HGas() {
  setTk(298.15);
}
C2HGas::C2HGas(double ltk) {
  setTk(ltk);
}
void C2HGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&C2Hlt);
  }
  else {
    setData(&C2Hht);
  }
  return;
}
    
