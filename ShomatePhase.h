/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ShomatePhase.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/* ShomatePhase header file
 * generic data class for calculating thermodynamic data from Shomate equations
 * Victor Kress
 */
#ifndef SHOMATEPHASE_H
#define SHOMATEPHASE_H

#include "Phase.h"

/**
   structure to hold data for ShomatePhase class.  Form often used by NIST
   $Id: ShomatePhase.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
   @author Victor Kress
   $Revision: 1.1.1.1 $
   @see ShomatePhase
*/
struct ShomateData{
  /// name of referenced phase
  char *name;
  /// formula of referenced phase
  char *formula;
  /// molecular weight of referenced phase
  double mw;
  /// Shomate A
  double A;
  /// Shomate B
  double B;
  /// Shomate C
  double C;
  /// Shomate D
  double D;
  /// Shomate E
  double E;
  /// Shomate F
  double F;
  /// Shomate G
  double G;
  /// Enthalpy of formation at STP (kJ)
  double Hf;
};

/**
   Generic Phase using Shomate formulation.  Form often used by NIST.
   only defined at one bar
   @author Victor Kress
   @version 1.0
   @see ShomateData
*/
class ShomatePhase: public Phase {
 public:
  /// generic constructor
  ShomatePhase();
  /// constructor using ShomateData structure.
  ShomatePhase(ShomateData *sd);

  /// set temperature in Kelvins
  virtual void setTk(double ltk);  // Overrides Phase
  /// set new thermo data
  void setData(ShomateData *sd);    // set data

  /// apparent Gibbs free energy
  virtual double getGibbs();
  /// apparent enthalpy
  virtual double getEnthalpy();
  /// third law entropy
  virtual double getEntropy();
  /// heat capacity
  virtual double getCp();
  /// derivative of Cp with respect to T
  virtual double getdCpdT();
 protected:
  /// reduced temperature (=tk/1000)
  double t;                        // tk/1000
  /// class data set
  ShomateData *dat;
};

#endif



