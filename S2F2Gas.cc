/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: S2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * S2F2Gas implementation
 * $Id: S2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "S2F2Gas.h"

static ShomateData S2F2lt = {"S2F2 gas","S2F2",102.1280,
			     69.74090,23.95960,-15.28380,3.293710,-1.121800,
			     -426.9130,364.3920,-401.4130};
static ShomateData S2F2ht = {"S2F2 gas","S2F2",102.1280,
			     82.69760,0.313834,-0.083492,0.007953,-2.371711,
			     -433.5430,380.9120,-401.4130};
// note: static in this case makes it local to this file

S2F2Gas::S2F2Gas() {
  setTk(298.15);
}

S2F2Gas::S2F2Gas(double ltk) {
  setTk(ltk);
}

void S2F2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<2000.0) {
    setData(&S2F2lt);
  }
  else {
    setData(&S2F2ht);
  }
  return;
}



