/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Cl2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * Cl2SGas implementation
 * JANAF Chase (1998)
 * 800-6000 
 * $Id: Cl2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "Cl2SGas.h"

static ShomateData Cl2S = {"Cl2S gas","Cl2S",102.9720,
			   57.21411,0.943120,-0.307046,0.034359,-0.596605,
			   -36.67096,347.2134,-17.57301};
// note: static in this case makes it local to this file

Cl2SGas::Cl2SGas() {
  setData(&Cl2S);
}

Cl2SGas::Cl2SGas(double ltk) {
  setData(&Cl2S);
  setTk(ltk);
}




