/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Phase.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "Phase.h"
#include <stdio.h>
#include <string.h>

/* Constructors **********************/
Phase::Phase():R(8.31468),tr(298.15) {
  ncomp=1;  //default value if not changed by daughter
  setTk(tr);
  setMoles(1.0);
  return;
}
Phase::~Phase() {
  return;
}

/* getting persistent properties *****************/
int Phase::getNcomp() {
  return 1;
}
double Phase::getMW() {
  return mw;
}
char *Phase::getCompName(int i) {
  if (i==0) {
    return name;
  }
  else {
    return NULL;
  }
}
char *Phase::getCompFormula(int i) {
  if (i==0) {
    return formula;
  }
  else {
    return NULL;
  }
}
int Phase::getCompNo(char *lab) {
  if (strcmp(name,lab)==0) {
      return 0;
  }
  else {
    return -1;
  }
}

/* Setting protected variables *******************/
void Phase::setTk(double ltk) {
  tk = ltk;
  return;
}
void Phase::setMoles(double lm) {
  nmoles = lm;
  return;
}

/* Getting methods ****************/
double Phase::getTk() {
  return tk;
}
double Phase::getMoles() {
  return nmoles;
}

// Gibbs and derivatives
double Phase::getGibbs() {
  return 0.;
}
double Phase::getMu0(int i) {
  if (i==0) {
    return getGibbs()/nmoles;
  }
  else {
    return 0.;
  }
}
void Phase::getMu0(double *m0) {
  m0[0]=getMu0(0);
  return;
}
double Phase::getMu(int i) {
  return getMu0(i);
}
void Phase::getMu(double *mu) {
  mu[0]=getMu(0);
  return;
}

// utility methods //////////////////////////////
int Phase::init() {
  return 0;
}



