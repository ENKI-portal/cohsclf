/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: S2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * S2OGas implementation
 * $Id: S2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "S2OGas.h"

static ShomateData s2o = {"S2O gas","S2O",79.7984,
			  52.74890,4.576460,-1.232311,0.104897,-0.942963,
			  -75.59880,324.1110,-56.48400};

S2OGas::S2OGas() {
  setData(&s2o);
}
S2OGas::S2OGas(double ltk) {
  setData(&s2o);
  setTk(ltk);
}
