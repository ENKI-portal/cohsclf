/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: HFGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * HFGas implementation
 * $Id: HFGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "HFGas.h"

static ShomateData HFlt = {"HF gas","HF",20.0059,
			   30.11693,-3.246612,2.868116,0.457914,-0.024861,
			   -281.4912,210.9226,-272.5462};
static ShomateData HFht = {"HF gas","HF",20.0059,
			   24.57033,6.893391,-1.243874,0.082583,-0.234060,
			   -279.7653,202.8525,-272.5462};
// note: static in this case makes it local to this file

HFGas::HFGas() {
  setTk(298.15);
}

HFGas::HFGas(double ltk) {
  setTk(ltk);
}

void HFGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&HFlt);
  }
  else {
    setData(&HFht);
  }
  return;
}



