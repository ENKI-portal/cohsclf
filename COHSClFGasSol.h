/*
 *   This file is part of a software package for calculating homogeneous equilibrium in
 *   C-O-H-S-Cl-F gasses at one bar.
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003)
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COHSClFGasSol.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#ifndef COHSCLFGASSOL_H
#define COHSCLFGASSOL_H

#include "SpecSolution.h"

/**
   COHSClF gas class.  Employs JANAF-NIST data from Chase (1998). 
   Ideal mixing assumed.
   $Id: COHSClFGasSol.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
   @author Victor Kress
   @version $Revision: 1.1.1.1 $
*/
class COHSClFGasSol: public SpecSolution {

 public:
  /// number of components
  const int NCOMP;
  /// number of species
  const int NSPEC;
  /// carbon
  const int CDEX;
  /// oxygen
  const int O2DEX;
  /// hydrogen
  const int H2DEX;
  /// S2
  const int S2DEX;
  /// chlorine
  const int CL2DEX;
  /// flourine
  const int F2DEX;
  /// S
  const int SDEX;
  /// S3
  const int S3DEX;
  /// carbon monoxide
  const int CODEX;
  /// carbon dioxide
  const int CO2DEX;
  /// sulfur dioxide
  const int SO2DEX;
  /// SO
  const int SODEX;
  /// S2O
  const int S2ODEX;
  /// COS
  const int COSDEX;
  /// CS
  const int CSDEX;
  /// CS2
  const int CS2DEX;
  /// H
  const int HDEX;
  /// OH
  const int OHDEX;
  /// HO2
  const int HO2DEX;
  /// water
  const int H2ODEX;
  /// H2O2
  const int H2O2DEX;
  /// H2SO4
  const int H2SO4DEX;
  /// H2S
  const int H2SDEX;
  /// CH
  const int CHDEX;
  /// COH
  const int COHDEX;
  /// CH2
  const int CH2DEX;
  /// COH2
  const int COH2DEX;
  /// CH3
  const int CH3DEX;
  /// CH4
  const int CH4DEX;
  /// C2H
  const int C2HDEX;
  /// C2H2
  const int C2H2DEX;
  /// C2H4
  const int C2H4DEX;
  /// C2OH4
  const int C2OH4DEX;
  /// C2O
  const int C2ODEX;
  /// C3O2
  const int C3O2DEX;
  /// HS
  const int HSDEX;
  /// SO3
  const int SO3DEX;
  /// hydrochloric acid
  const int HCLDEX;
  /// dichlorine monoxide
  const int CL2ODEX;
  /// monochlorine monoxide
  const int CLODEX;
  /// chlorine dioxide
  const int CLO2DEX;
  /// sulfur dichloride
  const int CL2SDEX;
  /// disulfur dichloride
  const int CL2S2DEX;
  /// sulfur chloride
  const int CLSDEX;
  /// disulfur chloride
  const int CLS2DEX;
  /// ethyne dichloro
  const int C2CL2DEX;
  /// tetrachloroethylene
  const int C2CL4DEX;
  /// carbon tetrachloride
  const int CCL4DEX;
  /// hydrogen fluoride dimer
  const int H2F2DEX;
  /// hydrofluoric acid
  const int HFDEX;
  /// difluorine monoxide
  const int F2ODEX;
  /// oxygen monofuoride
  const int FODEX;
  /// sulfur difluoride
  const int F2SDEX;
  /// difluoro disulfide
  const int F2S2DEX;
  /// thiothionyl fluoride
  const int S2F2DEX;
  /// sulfur trifluoride
  const int F3SDEX;
  /// sulfur tetrafluoride
  const int F4SDEX;
  /// ethyne difluoro
  const int C2F2DEX;
  /// ethyne tetrafluoro
  const int C2F4DEX;
  /// carbon tetrafluoride
  const int CF4DEX;
  /// dichlorodifluoromethane
  const int CCL2F2DEX;
  /// trichloromonofluoromethane
  const int CCL3FDEX;
  /// chlorotrifluoromethane
  const int CCLF3DEX;
  /// carbonic chloride fluoride
  const int CCLFODEX;
  /// methane dichlorofluoro
  const int CHCL2FDEX;
  /// methane chlorodifluoro
  const int CHCLF2DEX;
  /// chlorine monofluoride
  const int CLFDEX;
  /// sulfuryl chloride fruoride
  const int CLFO2SDEX;

  /// constructor
  COHSClFGasSol();
  /// destructor
  virtual ~COHSClFGasSol();
  
 protected:
  /// allocate space and initialize variables
  virtual int init();

};

#endif


