/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Cl2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * Cl2Gas implementation
 * $Id: Cl2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "Cl2Gas.h"

static ShomateData Cl2lt = {"Cl2 gas","Cl2",70.9060,
			    33.05060,12.22940,-12.06510,4.385330,-0.159494,
			    -10.83480,259.0290,0.000000};
static ShomateData Cl2mt = {"Cl2 gas","Cl2",70.9060,
			    42.67730,-5.009570,1.904621,-0.165641,-2.098480,
			    -17.28980,269.8400,0.000000};
static ShomateData Cl2ht = {"Cl2 gas","Cl2",70.9060,
			    -42.55350,41.68570,-7.126830,0.387839,101.1440,
			    132.7640,264.7860,0.000000};
// note: static in this case makes it local to this file

Cl2Gas::Cl2Gas() {
  setTk(298.15);
}

Cl2Gas::Cl2Gas(double ltk) {
  setTk(ltk);
}

void Cl2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&Cl2lt);
  }
  else if (tk<3000.0) {
    setData(&Cl2mt);
  }
  else {
    setData(&Cl2ht);
  }
  return;
}




