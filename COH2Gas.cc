/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: COH2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * COH2Gas implementation
 * $Id: COH2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "COH2Gas.h"

static ShomateData COH2lt = {"COH2 gas","COH2",30.02628,
			     5.193767,93.23249,-44.85457,7.882279,0.551175,
			     -119.3591,202.4663,-115.8972};
static ShomateData COH2ht = {"COH2 gas","COH2",30.02628,
			     71.35268,6.174497,-1.191090,0.079564,-15.58917,
			     -170.6327,262.3180,-115.8972};

COH2Gas::COH2Gas() {
  setTk(298.15);
}
COH2Gas::COH2Gas(double ltk) {
  setTk(ltk);
}
void COH2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&COH2lt);
  }
  else {
    setData(&COH2ht);
  }
  return;
}
    
