/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: SO3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * SO3Gas implementation
 * $Id: SO3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "SO3Gas.h"

static ShomateData SO3lt = {"SO3 gas","SO3",80.0642,
			    24.02503,119.4607,-94.38686,26.96237,-0.117517,
			    -407.8526,253.5186,-395.7654};
static ShomateData SO3ht = {"SO3 gas","SO3",80.0642,
			    81.99008,0.622236,-0.122440,0.008294,-6.703688,
			    -437.6590,330.9264,-395.7654};

SO3Gas::SO3Gas() {
  setTk(298.15);
}
SO3Gas::SO3Gas(double ltk) {
  setTk(ltk);
}
void SO3Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&SO3lt);
  }
  else {
    setData(&SO3ht);
  }
  return;
}
    
