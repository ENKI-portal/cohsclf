# $Id: Makefile,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
# makefile for COHSCLF to make sure everything compiles correctly
#
#   This file is part of a software package for calculating homogeneous
#   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
#   Copyright (C) 2003  Victor Kress
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   If you use all or part of this software, please reference the published
#   description in:
#       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
#       Microsoft EXCEL spreadsheet-based program for calculating
#       equilibrium gas speciation in the C-O-H-S-Cl-F system.
#       Computers & Geosciences.
#   (full reference in accompanying file "reference.txt")
#
#

PHASES =COHSClFGasSol.cc Phase.cc ShomatePhase.cc Solution.cc SpecSolution.cc \
	CGas.cc O2Gas.cc H2Gas.cc S2Gas.cc Cl2Gas.cc F2Gas.cc \
	SGas.cc S3Gas.cc COGas.cc CO2Gas.cc SO2Gas.cc SOGas.cc \
	S2OGas.cc COSGas.cc CSGas.cc CS2Gas.cc HGas.cc OHGas.cc \
	HO2Gas.cc H2OGas.cc H2O2Gas.cc H2SO4Gas.cc H2SGas.cc CHGas.cc \
	COHGas.cc CH2Gas.cc COH2Gas.cc CH3Gas.cc CH4Gas.cc C2HGas.cc \
	C2H2Gas.cc C2H4Gas.cc C2OH4Gas.cc C2OGas.cc C3O2Gas.cc HSGas.cc \
	SO3Gas.cc C2F2Gas.cc C2Cl4Gas.cc C2Cl2Gas.cc S2F2Gas.cc HFGas.cc \
	HClGas.cc H2F2Gas.cc FOGas.cc F4SGas.cc F3SGas.cc F2SGas.cc \
	F2S2Gas.cc F2OGas.cc ClSGas.cc ClS2Gas.cc ClOGas.cc ClO2Gas.cc \
	Cl2SGas.cc Cl2S2Gas.cc Cl2OGas.cc CF4Gas.cc CCl4Gas.cc C2F4Gas.cc \
	ClFO2SGas.cc ClFGas.cc CHClF2Gas.cc CHCl2FGas.cc CClFOGas.cc \
	CClF3Gas.cc CCl3FGas.cc CCl2F2Gas.cc

#CMATH = NumUtil.cc svdSolve.cc svdInv.cc svDcmp.cc ModifiedCholesky.cc 

OBJ = $(addsuffix .o,$(basename $(PHASES)))

CMATHDIR = $(HOME)/projects/cvsct/cmath/

DEPS = $(addsuffix .d,$(basename $(PHASES)))


.SUFFIXES : .d

.cc.d .c.d :
	$(SHELL) -ec '$(CC) -I$(CMATHDIR) -MM $(CPPFLAGS) $<  > $@; \
	[ -s $@ ] || rm -f $@'

CXX = g++
CC = g++

CPPFLAGS = -c -I$(CMATHDIR) -fwritable-strings -Wno-deprecated 

all: $(OBJ)
	g++ -o $@ $@.o $(OBJ) -lc -lm

include $(DEPS)







