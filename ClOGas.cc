/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClOGas implementation
 * $Id: ClOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClOGas.h"

static ShomateData ClOlt = {"ClO gas","ClO",51.4520,
			    18.71767,59.07975,-73.82291,34.56222,0.076445,
			    93.85298,235.0906,101.2189};
static ShomateData ClOht = {"ClO gas","ClO",51.4520,
			    36.99648,0.748572,-0.067520,0.005234,-0.787617,
			    87.75229,267.4350,101.2189};

ClOGas::ClOGas() {
  setTk(298.15);
}
ClOGas::ClOGas(double ltk) {
  setTk(ltk);
}
void ClOGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<600.0) {
    setData(&ClOlt);
  }
  else {
    setData(&ClOht);
  }
  return;
}
    
