/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C3O2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C3O2Gas implementation
 * $Id: C3O2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C3O2Gas.h"

static ShomateData C3O2lt = {"C3O2 gas","C3O2",68.0318,
			    51.83223,82.07920,-44.90311,9.130786,-0.494976,
			     -114.0219,313.4544,-93.63792};
static ShomateData C3O2ht = {"C3O2 gas","C3O2",68.0318,
			     106.5761,3.008431,-0.585714,0.039383,-12.06958,
			     -153.2486,367.6627,-93.63792};

C3O2Gas::C3O2Gas() {
  setTk(298.15);
}
C3O2Gas::C3O2Gas(double ltk) {
  setTk(ltk);
}
void C3O2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&C3O2lt);
  }
  else {
    setData(&C3O2ht);
  }
  return;
}
    
