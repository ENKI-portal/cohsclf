/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CCl3FGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CCl3FGas implementation
 * $Id: CCl3FGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CCl3FGas.h"

static ShomateData CCl3Flt = {"CCl3F gas","CCl3F",137.3680,
			      34.06650,230.4309,-289.4558,135.5248,-0.232263,
			      -307.5847,292.6202,-288.6964};
static ShomateData CCl3Fht = {"CCl3F gas","CCl3F",137.3680,
			      106.2694,1.245277,-0.292652,0.022658,-3.710084,
			      -331.8887,419.8728,-288.696};
// note: static in this case makes it local to this file

CCl3FGas::CCl3FGas() {
  setTk(298.15);
}

CCl3FGas::CCl3FGas(double ltk) {
  setTk(ltk);
}

void CCl3FGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<600.0) {
    setData(&CCl3Flt);
  }
  else {
    setData(&CCl3Fht);
  }
  return;
}




