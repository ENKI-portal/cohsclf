/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: H2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * H2SGas implementation
 * $Id: H2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "H2SGas.h"

static ShomateData H2Slt = {"H2S gas","H2S",34.08188,
			    26.88412,18.67809,3.434203,-3.378702,0.135882,
			    -28.91211,233.3747,-20.50202};
static ShomateData H2Sht = {"H2S gas","H2S",34.08188,
			    51.22136,4.147486,-0.643566,0.041621,-10.46385,
			    -55.87606,243.6900,-20.50202};

H2SGas::H2SGas() {
  setTk(298.15);
}
H2SGas::H2SGas(double ltk) {
  setTk(ltk);
}
void H2SGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1400.0) {
    setData(&H2Slt);
  }
  else {
    setData(&H2Sht);
  }
  return;
}
    
