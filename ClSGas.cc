/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClSGas implementation
 * JANAF Chase (1998)
 * 800-6000 
 * $Id: ClSGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClSGas.h"

static ShomateData ClS = {"ClS gas","ClS",67.5190,
			  38.37456,-0.379941,0.163914,-0.007827,-0.053801,
			  144.8580,283.5965,156.4653};
// note: static in this case makes it local to this file

ClSGas::ClSGas() {
  setData(&ClS);
}

ClSGas::ClSGas(double ltk) {
  setData(&ClS);
  setTk(ltk);
}




