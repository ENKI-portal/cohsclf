/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CCl4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CCl4Gas implementation
 * JANAF Chase (1998)
 * 298-6000 
 * $Id: CCl4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CCl4Gas.h"

static ShomateData CCl4 = {"CCl4 gas","CCl4",153.8230,
			   103.1134,4.188644,-1.126475,0.095677,-1.919624,
			   -133.3357,422.4334,-95.98096};
// note: static in this case makes it local to this file

CCl4Gas::CCl4Gas() {
  setData(&CCl4);
}

CCl4Gas::CCl4Gas(double ltk) {
  setData(&CCl4);
  setTk(ltk);
}




