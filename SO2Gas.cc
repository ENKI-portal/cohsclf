/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: SO2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * SO2Gas implementation
 */

#include "SO2Gas.h"

static ShomateData SO2lt = {"SO2 gas","SO2",64.0648,
			    21.43049,74.35094,-57.75217,16.35534,0.086731,
			    -305.7688,254.8872,-296.8422};
static ShomateData SO2ht = {"SO2 gas","SO2",64.0648,
			    57.48188,1.009328,-0.076290,0.005174,-4.045401,
			    -324.4140,302.7798,-296.8422};

SO2Gas::SO2Gas() {
  setTk(298.15);
}
SO2Gas::SO2Gas(double ltk) {
  setTk(ltk);
}
void SO2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1200.0) {
    setData(&SO2lt);
  }
  else {
    setData(&SO2ht);
  }
  return;
}
    
