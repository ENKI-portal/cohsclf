/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClS2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClS2Gas implementation
 * JANAF Chase (1998)
 * 298-6000 
 * $Id: ClS2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClS2Gas.h"

static ShomateData ClS2 = {"ClS2 gas","ClS2",99.5850,
			   57.20365,0.993775,-0.353668,0.045370,-0.591505,
			   59.47933,357.2734,78.55920};
// note: static in this case makes it local to this file

ClS2Gas::ClS2Gas() {
  setData(&ClS2);
}

ClS2Gas::ClS2Gas(double ltk) {
  setData(&ClS2);
  setTk(ltk);
}




