/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: HGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * HGas implementation
 * $Id: HGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "HGas.h"

static ShomateData h = {"H gas","H",1.00794,
			20.78603,4.850638e-10,-1.582916e-10,
			1.525102e-11,3.196347e-11,211.8020,139.8711,
			217.9994};

HGas::HGas() {
  setData(&h);
}
HGas::HGas(double ltk) {
  setData(&h);
  setTk(ltk);
}
