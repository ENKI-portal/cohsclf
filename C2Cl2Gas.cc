/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2Cl2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2Cl2Gas implementation
 * $Id: C2Cl2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2Cl2Gas.h"

static ShomateData C2Cl2lt = {"C2Cl2 gas","C2Cl2",94.9280,
			      61.94956,35.38417,-20.38968,4.488973,-0.465554,
			      188.1846,334.6924,209.6184};
static ShomateData C2Cl2ht = {"C2Cl2 gas","C2Cl2",94.9280,
			      84.88081,1.284146,-0.249917,0.016798,-5.244267,
			      171.9524,357.8454,209.6184};
// note: static in this case makes it local to this file

C2Cl2Gas::C2Cl2Gas() {
  setTk(298.15);
}

C2Cl2Gas::C2Cl2Gas(double ltk) {
  setTk(ltk);
}

void C2Cl2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1200.0) {
    setData(&C2Cl2lt);
  }
  else {
    setData(&C2Cl2ht);
  }
  return;
}



