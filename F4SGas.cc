/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: F4SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * F4SGas implementation
 * $Id: F4SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "F4SGas.h"

static ShomateData F4Slt = {"F4S gas","F4S",108.0580,
			    29.37896,261.1678,-345.0218,168.1370,-0.305321,
			    -781.8390,269.4630,-763.1616};
static ShomateData F4Sht = {"F4S gas","F4S",108.0580,
			    106.7707,0.905459,-0.213326,0.016546,-3.601403,
			    -806.3070,410.6019,-763.1616};
// note: static in this case makes it local to this file

F4SGas::F4SGas() {
  setTk(298.15);
}

F4SGas::F4SGas(double ltk) {
  setTk(ltk);
}

void F4SGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<600.0) {
    setData(&F4Slt);
  }
  else {
    setData(&F4Sht);
  }
  return;
}



