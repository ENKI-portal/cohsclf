/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: Phase.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/* Phase header file
 * Generic phase class.  Parent to all phases.
 * Victor Kress
 */
#ifndef PHASE_H
#define PHASE_H

/**
   Generic Phase class. Parent class to all Phases.
   SI units throughout with Tr=273.15.
   $Id: Phase.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
   @author Victor Kress
   @version $Revision: 1.1.1.1 $
*/
class Phase {
 public:
  /// blank constructor.  STP assumed.
  Phase(void);
  /// generic destructor
  virtual ~Phase();

  // persistent phase properties //////
  /// name of phase.
  char *name; 
  /// formula of phase.
  char *formula;
  /// Returns number of components (1 for base class)
  virtual int getNcomp(void);
  /// Return molecular weight for phase (grams/mol)
  virtual double getMW(void);
  /// gets component number for input string.  Match must be exact.
  /// @param lab char* to name of phase to match
  /// @return integer index or -1 if not found.
  virtual int getCompNo(char *lab);
  /// get component name corresponding to string.
  /// Caution: string destroyed with Phase object.
  /// @param i integer component number.
  /// @return pointer to component name string or NULL if not found. 
  virtual char *getCompName(int i);
  /// get component formula.
  /// Caution: string destroyed with Phase object.
  /// @param i integer component number.
  /// @return pointer to component formula or NULL if not found. 
  virtual char *getCompFormula(int i);

  // setting methods ////////////////////
  /// set temperature in Kelvins.
  virtual void setTk(double ltk);
  /// set total number of moles.
  virtual void setMoles(double lm);

  // getting methods /////
  /// return temperature in Kelvins
  virtual double getTk();
  /// return total number of moles
  virtual double getMoles();

  // Gibbs and derivatives
  /// Gibbs free energy in Joules
  virtual double getGibbs();
  /// standard state chemical potential in Joules.
  /// @param i component number
  /// @return potential in joules or zero if not found.
  virtual double getMu0(int i);
  /// standard state chemical potential in Joules.
  /// @return potential of all components in joules.
  virtual void getMu0(double *m0);
  /// standard state chemical potential in Joules. Same as getMu0 for 
  /// single-component phase.
  /// @param i component number
  /// @return potential in joules or zero if not found.
  virtual double getMu(int i);
  /// chemical potential in Joules.
  /// @return potential of all components in joules.
  virtual void getMu(double *m);

 protected:
  /// number of components
  int ncomp;
  /// dummy initialization function
  virtual int init();
  /// temperature in Kelvins
  double tk;
  /// total number of moles
  double nmoles;
  /// molecular weight in grams
  double mw; 
  /// J/Kmol
  const double R;
  /// reference temperature
  const double tr;
};

#endif
