/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CHGas implementation
 * $Id: CHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CHGas.h"

static ShomateData CHlt = {"CH gas","CH",13.01894,
			   32.94210,-16.71056,24.18595,-7.784709,-0.065198,
			   584.6303,226.5138,594.1280};
static ShomateData CHht = {"CH gas","CH",13.01894,
			   30.15367,8.455112,-1.969644,0.154270,-4.980090,
			   576.6891,209.3531,594.1280};

CHGas::CHGas() {
  setTk(298.15);
}
CHGas::CHGas(double ltk) {
  setTk(ltk);
}
void CHGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1100.0) {
    setData(&CHlt);
  }
  else {
    setData(&CHht);
  }
  return;
}
    
