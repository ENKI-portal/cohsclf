/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: NumUtil.h,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * NumUtil file
 * includes various numerical utilities
 * Victor Kress
 */
#ifndef NUMUTIL_H
#define NUMUTIL_H

/// implements dirac delta function
#define DIRAC(i,j) ((i==j)?1.0:0.0)
/// squares argument
#define SQR(x) ((x)*(x))
/// cubes argument
#define CUB(x) ((x)*(x)*(x))

/// allocates ** double matrix
double **dmatrix(int m,int n); 
/// deletes dmatrix          
void free_dmatrix(int m,int n,double **a);
/// allocates 3d matrix
double ***dmatrix3d(int m,int n,int o);
/// deletes 3d matrix
void free_dmatrix3d(int m,int n,int o,double ***a);
/// allocates ** int matrix
int **imatrix(int m,int n); 
/// deletes imatrix
void free_imatrix(int m,int n,int **a);
/// convert c++ double matrix to **
double **conv_dmatrix(int m,int n,double **x);
/// returns double identity matrix
double **doubleIdentityMatrix(int m);
/// for backward compatibility
/// allocates vector of double
/// @param i dimension of vector
double *dvector(int i);    
/// for backward compatibility
/// deallocates vector created with dvector
void free_dvector(double *v,int i);    
/// for backward compatibility
/// allocates vector of int
/// @param i dimension of vector
int *ivector(int i);    
/// for backward compatibility
/// deallocates vector created with ivector
void free_ivector(int *v,int i);    

#endif




