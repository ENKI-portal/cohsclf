/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: NumUtil.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * NumUtil routine bodies
 * prototypes in NumUtil.h
 * Victor Kress
 */

#include "NumUtil.h"

double **dmatrix(int m,int n) {
  int i;
  double **a;
  a = new (double*)[m];
  for(i=0;i<m;i++) {
    a[i] = new double[n];
  }
  return a;
}
  
void free_dmatrix(int m,int n,double **a) {
  int i;
  for(i=0;i<m;i++) {
    delete[] a[i];
  }
  delete[] a;
  a=0;
  return;
  }

double ***dmatrix3d(int m,int n,int o) {
  int i;
  double ***a;
  a = new (double**)[m];
  for (i=0;i<m;i++) {
    a[i]=dmatrix(n,o);
  }
  return a;
}

void free_dmatrix3d(int m,int n,int o,double ***a) {
  int i;
  for (i=0;i<m;i++) {
    free_dmatrix(n,o,a[i]);
  }
  delete[] a;
  a=0;
  return;
}

int **imatrix(int m,int n) {
  int i;
  int **a;
  a = new (int*)[m];
  for(i=0;i<m;i++) {
    a[i] = new int[n];
  }
  return a;
}
  
void free_imatrix(int m,int n,int **a) {
  int i;
  for(i=0;i<m;i++) {
    delete[] a[i];
  }
  delete[] a;
  a=0;
  return;
}

double **conv_dmatrix(int m,int n,double **x) {
  int i;
  double **a;
  a = new (double*)[m];
  for (i=0;i<m;i++) {
    a[i] = *x+i*n;
  }
  return a;
}

double **doubleIdentityMatrix(int m) {
  int i,j;
  double **I;
  I=dmatrix(m,m);
  for(i=0;i<m;i++) {
    for (j=0;j<m;j++) {
      I[i][j] = (i==j)? 1.0 : 0.0;;
    }
  }
  return I;
}

double *dvector(int n) {
  double *v;
  v = new double[n];
  return v;
}

void free_dvector(double *v,int n) {
  delete []v;
  return;
}

int *ivector(int n) {
  int *v;
  v = new int[n];
  return v;
}

void free_ivector(int *v,int n) {
  delete []v;
  return;
}

