/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CH3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CH3Gas implementation
 * $Id: CH3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CH3Gas.h"

static ShomateData CH3lt = {"CH3 gas","CH3",15.03482,
			    28.13786,36.74736,-4.347218,-1.595674,0.001860,
			    135.7118,217.4814,145.6873};
static ShomateData CH3ht = {"CH3 gas","CH3",15.03482,
			    67.18081,7.846423,-1.440899,0.092685,-17.66133,
			    92.47100,235.9023,145.6873};

CH3Gas::CH3Gas() {
  setTk(298.15);
}
CH3Gas::CH3Gas(double ltk) {
  setTk(ltk);
}
void CH3Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1400.0) {
    setData(&CH3lt);
  }
  else {
    setData(&CH3ht);
  }
  return;
}
    
