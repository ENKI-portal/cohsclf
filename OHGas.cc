/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: OHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * OHGas implementation
 * $Id: OHGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "OHGas.h"

static ShomateData OHlt = {"OH gas","OH",17.00734,
			   32.27768,-11.36291,13.60545,-3.846486,-0.001335,
			   29.75113,225.5783,38.98706};
static ShomateData OHht = {"OH gas","OH",17.00734,
			   28.74701,4.714490,-0.814725,0.054748,-2.747829,
			   26.41439,214.1166,38.98706};

OHGas::OHGas() {
  setTk(298.15);
}
OHGas::OHGas(double ltk) {
  setTk(ltk);
}
void OHGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1300.0) {
    setData(&OHlt);
  }
  else {
    setData(&OHht);
  }
  return;
}
    
