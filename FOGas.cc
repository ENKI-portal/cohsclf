/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: FOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * FOGas implementation
 * $Id: FOGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "FOGas.h"

static ShomateData FOlt = {"FO gas","FO",34.9970,
			   20.78908,39.98971,-36.43344,12.19803,0.073183,
			   101.3515,231.8538,108.7840};
static ShomateData FOht = {"FO gas","FO",34.9970,
			   36.96798,0.962241,-0.105805,0.011789,-1.228854,
			   94.32117,256.1282,108.7840};
// note: static in this case makes it local to this file

FOGas::FOGas() {
  setTk(298.15);
}

FOGas::FOGas(double ltk) {
  setTk(ltk);
}

void FOGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&FOlt);
  }
  else {
    setData(&FOht);
  }
  return;
}



