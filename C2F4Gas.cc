/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2F4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2F4Gas implementation
 * $Id: C2F4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2F4Gas.h"

static ShomateData C2F4lt = {"C2F4 gas","C2F4",100.0140,
			     43.55126,175.9079,-138.7331,40.35619,-0.381260,
			     -679.4983,303.9350,-658.5616};
static ShomateData C2F4ht = {"C2F4 gas","C2F4",100.0140,
			     129.9776,1.690918,-0.340087,0.023448,-10.83204,
			     -725.3048,417.1829,-658.5616};
// note: static in this case makes it local to this file

C2F4Gas::C2F4Gas() {
  setTk(298.15);
}

C2F4Gas::C2F4Gas(double ltk) {
  setTk(ltk);
}

void C2F4Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1100.0) {
    setData(&C2F4lt);
  }
  else {
    setData(&C2F4ht);
  }
  return;
}



