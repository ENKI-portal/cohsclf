   C++ Source code directory for COHSClF Excel spreadsheet program.

   This file is part of a software package for calculating homogeneous
   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
   Copyright (C) 2003  Victor Kress

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   If you use all or part of this software, please reference the published
   description in:
       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
       Microsoft EXCEL spreadsheet-based program for calculating
       equilibrium gas speciation in the C-O-H-S-Cl-F system.
       Computers & Geosciences.
   (full reference in accompanying file "reference.txt")

   Files for creating the Microsoft Excel addin were not included for
   reasons of copyright.  To recreate these, the user will need a valid
   copy of the Microsoft Developer's Kit, and a fairly advanced
   understanding of the "Microsoft Way of Doing Things."
   Numerical library routines adapted from:

       Press, Flannery, Teukolsky and Vetterling (1990)
       Numerical Recipes in C: The Art of Scientific Computing.
       Cambridge University Press, New York.   

   are also omitted from this collection for reasons of copyright.

   Executable Excell spreadsheets and compiled "add-ins" for Windows and
   Macintosh platforms are available at http://ctserver.uchicago.edu 
   and http://ctserver.ess.washington.edu

   We hope the included files will be helpful.  Best of luck and happy
   programing.


                                -Victor Kress and Mark Ghiorso

   $Id: README.txt,v 1.2 2003/10/16 04:28:40 kress Exp $
 
