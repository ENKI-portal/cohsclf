/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ShomatePhase.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/* ShomatePhase functions
 * Implements seven-term Shomate form for S and Cp
 * Victor Kress
 */ 

#include <math.h>
#include "ShomatePhase.h"

ShomatePhase::ShomatePhase() {}
ShomatePhase::ShomatePhase(ShomateData *sd) {
  setData(sd);
}

// setting methods /////////////////////////////////////
void ShomatePhase::setTk(double ltk) {
  tk=ltk;
  t=tk/1000.;
}
void ShomatePhase::setData(ShomateData *sd) {
  dat=sd;
  name=dat->name;
  formula=dat->formula;
  mw=dat->mw;
}

// getting methods ////////////////////////////////////
double ShomatePhase::getGibbs() {
  double g;
  g = getEnthalpy() - tk*getEntropy();
  return g;
}
double ShomatePhase::getEnthalpy() {
  double h;
  h=(dat->A)*t + (dat->B)*t*t/2. + (dat->C)*t*t*t/3. + (dat->D)*t*t*t*t/4. 
    - (dat->E)/t + dat->F;
  h*=1000.;  // This is an artifact of the Shomate form.  
  return nmoles*h; // Joules/mol
}
double ShomatePhase::getEntropy() {
  double s;
  s=(dat->A)*log(t) + (dat->B)*t + (dat->C)*t*t/2. + (dat->D)*t*t*t/3. 
    - (dat->E)/(2.*t*t) + dat->G;
  return nmoles*s; // Joules/(mol*K)
}
double ShomatePhase::getCp() {
  double cp;
  cp=(dat->A) + (dat->B)*t + (dat->C)*t*t + (dat->D)*t*t*t
    + (dat->E)/(t*t);
  return nmoles*cp; // Joules/(mole*K)
}
double ShomatePhase::getdCpdT() {
  // dCpdTk = dCpdt*dtdTk
  double dcpdt;
  dcpdt = (dat->B) + 2.*(dat->C)*t + 3.*(dat->D)*t*t - 2.*(dat->E)/(tk*tk*tk);
  dcpdt /= 1000.; //dtdTk
  return dcpdt;
}
 


