/*
 *   This file is part of a software package for calculating homogeneous 
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar.
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003)
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: SpecSolution.cc,v 1.2 2003/10/22 01:26:26 kress Exp $
 */

#include <iostream.h>
#include <float.h>
#include <math.h>
#include "SpecSolution.h"
#include "NumUtil.h"
#include "svdSolve.h"
#include "svdInv.h"

SpecSolution::SpecSolution() {
  // init must be called by implementing child after ncomp and nspec set
  return;
}
SpecSolution::~SpecSolution() {
  // delete constructs allocated in init()
  delete []specs;
  free_dmatrix(nspec,ncomp,spstoich);
  free_dmatrix(nspec,nspec-ncomp,nu);
  delete []rdx;
  delete []rhs;
  delete []psi;
  delete []b;
  delete []bm;
  delete []delta;
  delete []mu;
  free_dmatrix(nspec,nspec,m1);
  free_dmatrix(nspec,nspec,m2);
  free_dmatrix(nspec,nspec,m3);
  delete []v1;
  return;
}
int SpecSolution::init() {
  int err;
  spectol=1.e-12;
  err=Solution::init();
  err |= (nspec<=0);
  if (err) {
    return err;
  }
  else {
    specs = new double[nspec];
    spstoich = dmatrix(nspec,ncomp);
    nu = dmatrix(nspec,nspec-ncomp);
    rdx = new int[nspec-ncomp];
    rhs = new double[ncomp+1];
    psi = new double[ncomp+1];
    b = new double[ncomp];
    bm = new double[ncomp];
    delta = new double[nspec];
    mu = new double[nspec];
    m1 = dmatrix(nspec,nspec);
    m2 = dmatrix(nspec,nspec);
    m3 = dmatrix(nspec,nspec);
    v1 = new double[nspec];
    return 0;
  }
}

// persistent phase properties ////////////////
int SpecSolution::getNspec() {
  return nspec;
}
char *SpecSolution::getSpecName(int i) {
  return ss[i]->name;
}
char *SpecSolution::getSpecFormula(int i) {
  return ss[i]->formula;
}

// setting methods ////////////////////////////
void SpecSolution::setTk(double ltk) {
  int i;
  tk=ltk;
  for (i=0;i<nspec;i++) {
    ss[i]->setTk(tk);
  }
  return;
}
void SpecSolution::setMoles(double m) {
  int i;
  double oldNmoles;
  oldNmoles=getMoles();
  Solution::setMoles(m);
  for (i=0;i<nspec;i++) {
    specs[i] *= m/oldNmoles;
  }
  for (i=0,specSum=0.;i<nspec;i++) specSum += specs[i];
  return;
}
void SpecSolution::setComps(double *lcomps) {
  int i;
  for (i=0,nmoles=0.;i<ncomp;i++) {
    comps[i]=lcomps[i];
    nmoles += comps[i];
  }
  fillSpecs();
  index();
  getMoles();  // to set nmoles
  return;
}
void SpecSolution::setSpecs(double *modelspecs) {
  int i;
  for (i=0;i<nspec;i++) {
    specs[i]=modelspecs[i];
  }
  for (i=0,specSum=0.;i<nspec;i++) specSum += specs[i];
  fillComps();
  index();
  return;
}
void SpecSolution::setSpecsFull(double *modelspecs) {
  int i;
  for (i=0;i<nspec;i++) {
    specs[i]=modelspecs[i];
  }
  fillComps();
  fillSpecs();
  index();
  return;
}

// getting methods ////////////////////////////////////
void SpecSolution::getSpecs(double *modelspecs) {
  int i;
  for (i=0;i<nspec;i++) {
    modelspecs[i]=specs[i];
  }
}
double SpecSolution::getMu0(int i) {
  if (i>=0&&i<nspec) {
    return ss[i]->getGibbs();
  }
  else {
    cout << "\nspec index out of range error in SpecSolution::getMu0";
    return 0.;
  }
}
double SpecSolution::getSpecMu(int ispec) {
  // Assuming ideal solution for now
  double mu;
  if (ispec<0||ispec>nspec) {
    cout << "\nSpecSolution::getSpecMu recieved out of range species number";
    return 0.;
  }
  else {
    mu = (specs[ispec]>0.)? getMu0(ispec)+R*tk*log(specs[ispec]/specSum) : 0.;
    return mu;
  } 
}

// utility functions ///////////////////////////
int SpecSolution::speciate(double tol) {
  spectol = tol;
  return speciate();
}
int SpecSolution::speciate() {
  int niter,i;
  double tol;
  tol = spectol;
  do {
    niter = speciate_function(tol);
    if (niter==-2) {
      tol*=5.0;
    }
    else if ((niter>0)&&(niter<2)&&(tol>spectol)) {
      tol/=2.0;
    }
    else break;
  } while (tol<1000000.);

  /*wrap up*/
  for (i=0,specSum=0.;i<nspec;i++) specSum += specs[i];
  return niter;
}

// protected utility functions /////////////////////////////////////
int SpecSolution::speciate_function(double tol) {
  /*Implements RAND speciation algorithm as presented in:
   *
   *Smith and Missen (1982) Chemical Reaction Equilibrium Analysis:
   *Theory and Algorithms. Krieger.
   *
   *Gf is vector of standard state chemical potentials at T
   *spstoich[0:nspec-1][0:ncomp-1]
   *
   *IDEAL SOLUTION in species assumed
   */
  
  int i,j,k,niter,converged;
  double sum,omega,dGdw[2],mdel;
  const int max=5000;
  const double SMALL=1000.*DBL_EPSILON;

  sum=0.;

  /*********fill b*************************
  for(k=0;k<lcomp;k++){
    for(j=0,b[k]=0.0;j<lspec;j++){
      b[k]+=spstoich[indx[j]][indx[k]]*specs[indx[j]];
    }
  }
  */
  for (k=0;k<lcomp;k++) {
    b[k] = comps[indx[k]];
  }
  /**/

  /*****************main iteration loop*************************/
  tol=fabs(tol);
  mdel=10.*tol;
  for (niter=0;niter<=max&&mdel>tol;niter++) {
    for(k=0;k<lcomp;k++){
      for(j=0,bm[k]=0.0;j<lspec;j++){
	bm[k]+=spstoich[indx[j]][indx[k]]*specs[indx[j]];
      }
    }
    /*********calculate potentials****************/
    for(i=0,sum=0.;i<lspec;i++)sum+=specs[indx[i]];
    for(i=0;i<lspec;i++) mu[i]=ss[indx[i]]->getGibbs()
			   +R*tk*log(specs[indx[i]]/sum);

    /***********fill A and rhs***(1 offset for N.R.)*****************/
    /*Equation 6.3-26*/
    /*lhs*/
    for (j=0;j<lcomp;j++){
      for (i=0;i<lcomp;i++){
	for (k=0,A[j][i]=0.0;k<lspec;k++){
	  A[j][i] += spstoich[indx[k]][indx[i]]*spstoich[indx[k]][indx[j]]
	    *specs[indx[k]];
        }
      }
      A[j][lcomp]=bm[j]; /*this is the 'u' variable in Smith and Missen*/
    }
    /*rhs*/
    for (j=0;j<lcomp;j++){
      for (k=0,rhs[j]=0.0;k<lspec;k++) {
	rhs[j]+=spstoich[indx[k]][indx[j]]*specs[indx[k]]*mu[k]/(R*tk);
      }
      rhs[j] += b[j]-bm[j];
    }

    /*Equation 6.3-27*/
    for (i=0;i<lcomp;i++) A[lcomp][i]=bm[i];
    A[lcomp][lcomp]= -0.;/*inert species (nz) modifying 'u'*/
    for (k=0,rhs[lcomp]=0.0;k<lspec;k++){
      rhs[lcomp] += specs[indx[k]]*mu[k]/(R*tk);
    }

    /*************solve for Psi*******************************/
    svdSolve(A,psi,rhs,lcomp+1,lcomp+1,w,v,1.e-7);

    /*************calculate increment*(Eq. 6.3-24)*****************/
    for (j=0;j<lspec;j++){
      for (k=0,delta[j]=0.0;k<lcomp;k++) {
	delta[j]+=spstoich[indx[j]][indx[k]]*psi[k];
      }
      delta[j] += psi[lcomp]-mu[j]/(R*tk);
      delta[j] *= specs[indx[j]];
    }

    /*calculate factor necessary to prevent negative fractions*/
    for (j=0,omega=1.0;j<lspec;j++){
      if (delta[j]<0.&&specs[indx[j]]+delta[j]<SMALL) {
	double temp;
	temp=specs[indx[j]]*(1.e-7 - 1.)/delta[j];
	if(temp<0.){
	  printf("negative value for temp at line #%d\n",__LINE__);
	}
	if (temp<omega)omega=temp;
      }
    }
    if (omega<1.0){
      for(j=0;j<lspec;j++){
	delta[j]*=omega; /*to prevent iteration to negative fractions*/
      }
    }

    /*************calculate omega**********************************/
    /*see Smith and Missen p. 115*/
    for (i=0,sum=0.;i<lspec;i++) {
      sum += specs[indx[i]]+delta[i];
    }
    for (i=0,dGdw[0]=dGdw[1]=0.0;i<lspec;i++){
      dGdw[0]+=mu[i]*delta[i];
      dGdw[1]+=(ss[indx[i]]->getGibbs()
		+R*tk*log((specs[indx[i]]+delta[i])/sum))*delta[i];
    }
    if (dGdw[0]<0.0) {
      if (dGdw[1]<=0.0) omega=1.0;
      else omega=dGdw[0]/(dGdw[0]-dGdw[1]);
    }
    else {
      /*printf("\nWarning: speciation function not incrementing 'downhill'");*/
      omega=0.5;
    }

    /*************increment species********************************/
    for (i=0,mdel=0.0;i<lspec;i++) {
      double temp;
      if (specs[indx[i]]>10.*SMALL){  /*no tiny species in convergence test*/
	temp=fabs(delta[i])/specs[indx[i]];
	if (mdel<temp) mdel=temp;
      }
      specs[indx[i]] += omega*delta[i];
      if (specs[indx[i]]<=0.0){
	printf("\nAttempt to drive species %d negative at iteration %d",
	       i,niter);
	printf("\nSpec[%d]=%10.1e  delta=%10.1e",i,specs[indx[i]],delta[i]);
	specs[indx[i]]=SMALL;
      }
    }

  } /*end of main loop*/
  
  /*****************wrap up******************************/
  converged = (niter<=max);
  {  // mass balance correction loop
    double cf;
    for(k=0;k<lcomp;k++){
      for(j=0,bm[k]=0.0;j<lspec;j++){
	bm[k]+=spstoich[indx[j]][indx[k]]*specs[indx[j]];
      }
    }
    for (i=0,cf=0.;i<lcomp;i++) {
      double del;
      del = (bm[i]/b[i]);
      cf += del;
    }
    cf /= (double)lcomp;
    if (cf!=1.) {
      for (i=0;i<lspec;i++) {
	specs[indx[i]] /= cf;
      }
    }
  }
  return niter;
} /*End of SPECIATE function*/

/* fillSpecs *****************************************/
int SpecSolution::fillSpecs() {
  const double SMALL=1.e-7;
  int i,j,k,kmin;
  double min;
  for (i=0;i<ncomp;i++) {
    specs[i]=comps[i];
  }
  /* Put minimal value in relevant species. */
  for (j=ncomp;j<nspec;j++) {
    for (k=0,kmin=-1;k<ncomp;k++) {
      //loop checks to see if all comps present and find smallest
      if (spstoich[j][k]>0.) {	// component required for species
	if (specs[k]>0.) {	// species present
	  if (kmin<0) {
	    kmin=k;
	  }
	  else {
	    if (specs[k]<specs[kmin]) {
	      kmin=k;
	    }
	  }
	}
	else {			// required component not present
	  break;                // if so species will be set to zero
	}
      }
    }
    if (k==ncomp) {		// got through whole list without break
      // this means we should put small amount in this species
      if (specs[kmin]>100.*SMALL) {
	min = SMALL;
      }
      else {
	min = specs[kmin]/1000.;
      }
      specs[j]=min/spstoich[j][kmin];
      // and subtract appropriate amount from components
      for (k=0;k<ncomp;k++) {
	specs[k] -= min*spstoich[j][k]/spstoich[j][kmin];
      }
    }
    else {		        // zero species 
      specs[j]=0.;
    }
    for (i=0,specSum=0.;i<nspec;i++) specSum += specs[i];
  }
  return 0;
} /*end fillSpecs*/
/* fillComps ***********************************************/
void SpecSolution::fillComps() {
  int i,j;
  /* specs*spstoich to transform to components */
  for (i=0,nmoles=0.;i<ncomp;i++) {
    comps[i]=0.;
    for (j=0;j<nspec;j++) {
      comps[i] += (specs[j]*spstoich[j][i]);
    }
    nmoles += comps[i];
  }
  return;
}
/* index **************************************************/
void SpecSolution::index() {
  int i,j,r;
  for (i=lcomp=lspec=r=0;i<nspec;i++) {
    if (specs[i]>0.0) {
      indx[lspec] = i;
      lspec++;
      if (i<ncomp) {
	lcomp++;
      }
      else {
	rdx[r]=i-ncomp;
	r++;
      }
    }
  }
  /* Fill nu matrix (Smith and Misson section 2.3.3)
   * This must be done every time in case one spec has zeroed component
   * IMPORTANT: nu is indexed on non-zero reactions and species.
   */
  for (j=0;j<r;j++) {	        /* all reactions */
    for (i=0;i<lcomp;i++) {	/* components */
      nu[i][j]=-spstoich[indx[lcomp+j]][indx[i]];
    }
  }
  
  for (i=0;i<r;i++) {		/* fill lower part with identity */
    for (j=0;j<r;j++) {
      nu[i+lcomp][j] = (i==j)? 1.0:0.0;
    }
  }
  return;
}
