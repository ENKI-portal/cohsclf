/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: F2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * F2OGas implementation
 * $Id: F2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "F2OGas.h"

static ShomateData F2Olt = {"F2O gas","F2O",53.9950,
			    15.06976,136.2612,-172.8088,81.40767,0.071893,
			    15.57553,232.4333,24.51803};
static ShomateData F2Oht = {"F2O gas","F2O",53.9950,
			    57.26808,0.640466,-0.150702,0.011677,-1.898625,
			    1.585041,307.3976,24.51803};
// note: static in this case makes it local to this file

F2OGas::F2OGas() {
  setTk(298.15);
}

F2OGas::F2OGas(double ltk) {
  setTk(ltk);
}

void F2OGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<600.0) {
    setData(&F2Olt);
  }
  else {
    setData(&F2Oht);
  }
  return;
}



