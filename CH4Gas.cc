/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CH4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CH4Gas implementation
 * $Id: CH4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CH4Gas.h"

static ShomateData CH4lt = {"Methane","CH4",16.04276,
			    -0.703029,108.4773,-42.52157,5.862788,0.678565,
			    -76.84376,158.7163,-74.87310};
static ShomateData CH4ht = {"Methane","CH4",16.04276,
			    85.81217,11.26467,-2.114146,0.138190,-26.42221,
			    -153.5327,224.4143,-74.87310};

CH4Gas::CH4Gas() {
  setTk(298.15);
}
CH4Gas::CH4Gas(double ltk) {
  setTk(ltk);
}
void CH4Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1300.0) {
    setData(&CH4lt);
  }
  else {
    setData(&CH4ht);
  }
  return;
}
    
