/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CClF3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CClF3Gas implementation
 * $Id: CClF3Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CClF3Gas.h"

static ShomateData CClF3lt = {"CClF3 gas","CClF3",104.4580,
			      10.77116,266.0384,-298.3581,126.6547,-0.003404,
			      -720.5936,231.1915,-707.9328};
static ShomateData CClF3ht = {"CClF3 gas","CClF3",104.4580,
			      103.9222,2.837556,-0.664553,0.051336,-5.531122,
			      -755.6764,384.5083,-707.9328};
// note: static in this case makes it local to this file

CClF3Gas::CClF3Gas() {
  setTk(298.15);
}

CClF3Gas::CClF3Gas(double ltk) {
  setTk(ltk);
}

void CClF3Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<600.0) {
    setData(&CClF3lt);
  }
  else {
    setData(&CClF3ht);
  }
  return;
}




