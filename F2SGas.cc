/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: F2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * F2SGas implementation
 * $Id: F2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "F2SGas.h"

static ShomateData F2Slt = {"F2S gas","F2S",70.0620,
			    33.79492,62.68050,-61.65417,21.62843,-0.238158,
			    -309.8064,281.1188,-296.6464};
static ShomateData F2Sht = {"F2S gas","F2S",70.0620,
			    57.99150,0.121780,-0.025398,0.001801,-1.894960,
			    -319.5158,319.2434,-296.6464};
// note: static in this case makes it local to this file

F2SGas::F2SGas() {
  setTk(298.15);
}

F2SGas::F2SGas(double ltk) {
  setTk(ltk);
}

void F2SGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1000.0) {
    setData(&F2Slt);
  }
  else {
    setData(&F2Sht);
  }
  return;
}



