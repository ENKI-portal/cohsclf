/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: H2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * H2F2Gas implementation
 * $Id: H2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "H2F2Gas.h"

static ShomateData H2F2lt = {"H2F2 gas","H2F2",40.0118,
			     35.58910,50.55234,-28.01142,6.708458,-0.313996,
			     -586.3416,266.2555,-572.6641};
static ShomateData H2F2ht = {"H2F2 gas","H2F2",40.0118,
			     68.32974,6.950001,-1.229352,0.076663,-12.36493,
			     -616.1693,293.0398,-572.6641};
// note: static in this case makes it local to this file

H2F2Gas::H2F2Gas() {
  setTk(298.15);
}

H2F2Gas::H2F2Gas(double ltk) {
  setTk(ltk);
}

void H2F2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1500.0) {
    setData(&H2F2lt);
  }
  else {
    setData(&H2F2ht);
  }
  return;
}



