/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CS2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CS2Gas implementation
 * $Id: CS2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CS2Gas.h"

static ShomateData CS2lt = {"CS2 gas","CS2",76.143,
			    35.85391,52.49121,-40.83743,12.00155,-0.224831,
			    103.5030,266.1597,116.9432};
static ShomateData CS2ht = {"CS2 gas","CS2",76.143,
			    61.25292,1.378826,-0.140520,0.009284,-3.244044,
			    90.07106,299.4091,116.9432};

CS2Gas::CS2Gas() {
  setTk(298.15);
}
CS2Gas::CS2Gas(double ltk) {
  setTk(ltk);
}
void CS2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1000.0) {
    setData(&CS2lt);
  }
  else {
    setData(&CS2ht);
  }
  return;
}
    
