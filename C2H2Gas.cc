/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2H2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2H2Gas implementation
 * $Id: C2H2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2H2Gas.h"

static ShomateData C2H2lt = {"Acetylene gas","C2H2",26.03788,
			     40.68697,40.73279,-16.17840,3.669741,-0.658411,
			     210.7067,235.0052,226.7314};
static ShomateData C2H2ht = {"Acetylene gas","C2H2",26.03788,
			     67.47244,11.75110,-2.021470,0.136195,-9.806418,
			     185.4550,253.5337,226.7314};

C2H2Gas::C2H2Gas() {
  setTk(298.15);
}
C2H2Gas::C2H2Gas(double ltk) {
  setTk(ltk);
}
void C2H2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1100.0) {
    setData(&C2H2lt);
  }
  else {
    setData(&C2H2ht);
  }
  return;
}
    
