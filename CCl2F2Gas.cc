/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CCl2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CCl2F2Gas implementation
 * $Id: CCl2F2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CCl2F2Gas.h"

static ShomateData CCl2F2lt = {"CCl2F2 gas","CCl2F2",120.9130,
			       48.01014,139.1808,-124.3326,39.75147,-0.633834,
			       -513.2304,319.1028,-491.6200};
static ShomateData CCl2F2ht = {"CCl2F2 gas","CCl2F2",120.9130,
			       107.3635,0.404844,-0.082033,0.005689,-5.707394,
			       -539.7486,406.4660,-491.6200};
// note: static in this case makes it local to this file

CCl2F2Gas::CCl2F2Gas() {
  setTk(298.15);
}

CCl2F2Gas::CCl2F2Gas(double ltk) {
  setTk(ltk);
}

void CCl2F2Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1100.0) {
    setData(&CCl2F2lt);
  }
  else {
    setData(&CCl2F2ht);
  }
  return;
}




