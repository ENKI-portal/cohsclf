/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2OGas implementation
 * $Id: C2OGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2OGas.h"

static ShomateData C2Olt = {"C2O gas","C2O",40.0214,
			    34.22445,41.04408,-23.52973,5.537273,-0.129471,
			    274.3386,262.5142,286.6044};
static ShomateData C2Oht = {"C2O gas","C2O",40.0214,
			    60.64080,4.317511,-1.109681,0.082437,-8.056669,
			    252.4044,286.0283,286.6044};

C2OGas::C2OGas() {
  setTk(298.15);
}
C2OGas::C2OGas(double ltk) {
  setTk(ltk);
}
void C2OGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1300.0) {
    setData(&C2Olt);
  }
  else {
    setData(&C2Oht);
  }
  return;
}
    
