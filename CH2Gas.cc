/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: CH2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * CH2Gas implementation
 * $Id: CH2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "CH2Gas.h"

static ShomateData CH2lt = {"CH2 gas","CH2",14.02688,
			    31.96823,6.783603,12.51890,-5.696266,-0.031115,
			    376.3558,229.9150,386.3924};
static ShomateData CH2ht = {"CH2 gas","CH2",14.02688,
			    51.55901,3.876975,-0.649608,0.037901,-10.72589,
			    350.6715,232.3212,386.3924};

CH2Gas::CH2Gas() {
  setTk(298.15);
}
CH2Gas::CH2Gas(double ltk) {
  setTk(ltk);
}
void CH2Gas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1400.0) {
    setData(&CH2lt);
  }
  else {
    setData(&CH2ht);
  }
  return;
}
    
