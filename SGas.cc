/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * SGas implementation
 */

#include "SGas.h"

static ShomateData slt = {"S gas","S",32.066,
			  27.45968,-13.32784,10.06574,-2.662381,-0.055851,
			  269.1149,204.2955,276.9804};
static ShomateData sht = {"S gas","S",32.066,
			  16.55345,2.400266,-0.255760,0.005821,3.564793,
			  278.4356,194.5447,276.9804};

SGas::SGas() {
  setTk(298.15);
}
SGas::SGas(double ltk) {
  setTk(ltk);
}
void SGas::setTk(double tl){
  ShomatePhase::setTk(tl);
  if (tk<1400.0) {
    setData(&slt);
  }
  else {
    setData(&sht);
  }
  return;
}
    
