/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: H2O2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * H2O2Gas implementation
 * $Id: H2O2Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "H2O2Gas.h"

static ShomateData h2o2 = {"H2O2 gas","H2O2",34.0146,
			34.25667,55.18445,-35.15443,9.087440,-0.422157,
			-149.9098,257.0604,-136.1064};

H2O2Gas::H2O2Gas() {
  setData(&h2o2);
}
H2O2Gas::H2O2Gas(double ltk) {
  setData(&h2o2);
  setTk(ltk);
}
