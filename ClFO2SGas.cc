/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: ClFO2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * ClFO2SGas implementation
 * $Id: ClFO2SGas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "ClFO2SGas.h"

static ShomateData ClFO2Slt = {"ClFO2S gas","ClFO2S",118.5150,
			       48.72561,128.2237,-108.8455,33.52028,-0.586095,
			       -577.7727,324.8248,-556.4720};
static ShomateData ClFO2Sht = {"ClFO2S gas","ClFO2S",118.5150,
			       106.8861,0.669687,-0.135353,0.009367,-6.437879,
			       -605.8641,406.2710,-556.4720};
// note: static in this case makes it local to this file

ClFO2SGas::ClFO2SGas() {
  setTk(298.15);
}

ClFO2SGas::ClFO2SGas(double ltk) {
  setTk(ltk);
}

void ClFO2SGas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<2300.0) {
    setData(&ClFO2Slt);
  }
  else {
    setData(&ClFO2Sht);
  }
  return;
}




