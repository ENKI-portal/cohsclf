/*
 *   This file is part of a software package for calculating homogeneous
 *   equilibrium in C-O-H-S-Cl-F gasses at one bar. 
 *   Copyright (C) 2003  Victor Kress
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.

 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *   If you use all or part of this software, please reference the published
 *   description in:
 *       Victor C. Kress, Mark S. Ghiorso  and Coby Lastuka (2003) 
 *       Microsoft EXCEL spreadsheet-based program for calculating
 *       equilibrium gas speciation in the C-O-H-S-Cl-F system.
 *       Computers & Geosciences.
 *   (full reference in accompanying file "reference.txt")
 *
 *   $Id: C2Cl4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */
/*
 * C2Cl4Gas implementation
 * $Id: C2Cl4Gas.cc,v 1.1.1.1 2003/10/14 21:22:07 kress Exp $
 */

#include "C2Cl4Gas.h"

static ShomateData C2Cl4lt = {"C2Cl4 gas","C2Cl4",165.8340,
			      72.76227,130.9546,-111.9299,34.69766,-0.644470,
			      -41.18395,393.4684,-12.42602};
static ShomateData C2Cl4ht = {"C2Cl4 gas","C2Cl4",165.8340,
			      131.629,0.825549,-0.179961,0.013780,-6.485786,
			      -69.4029,476.3651,-12.42602};
// note: static in this case makes it local to this file

C2Cl4Gas::C2Cl4Gas() {
  setTk(298.15);
}

C2Cl4Gas::C2Cl4Gas(double ltk) {
  setTk(ltk);
}

void C2Cl4Gas::setTk(double ltk) {
  ShomatePhase::setTk(ltk);
  if (tk<1100.0) {
    setData(&C2Cl4lt);
  }
  else {
    setData(&C2Cl4ht);
  }
  return;
}



